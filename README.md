## CardsFX

CardsFX is a simple toolkit for creating graphing and diagramming programs using JavaFX.

See [docs](docs/index.md) or more [technical](docs/technical.md) talk.

### Dependencies

[FXToys](https://gitlab.com/ravendyne-javafx-stuff/com.ravendyne.fxtoys.git)
