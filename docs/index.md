## CardsFX

### What's this?

CardsFX is a simple toolkit for creating graphing and diagramming programs using JavaFX.

![][cardsfx show]

Its main focus is on providing base implementation for essential parts of a graph/diagram representation:

- UI representation of a graph/diagram main parts:
   + drawing space (DrawingBoard)
   + nodes (Card)
   + connections between nodes (Connector)
   + attachment points on nodes for the connections (Pin)
- node positioning on the board by dragging it with mouse/touch gesture
- implementation of drag-and-connect gesture for connecting nodes by connections
- multiple (group) node selection and positioning on board with mouse/touch gesture

There are a few sample implementations for nodes, connections and pins provided with the library and these are mainly meant as a samples and quick start pieces, not as a complete implementations for production use.

In order to be able to quickly and more easily use CardsFX, it may be helpful to take a look at the basic concepts behind the implementation and terms used througout the toolkit.

### Basic concepts

Basic terms used throughout CardsFX are:

- Drawing board
- Card
- Connector
- Pin

These are implemented in CardsFX as abstract classes and in order to be able to use any of those in your application, you will have to subclass the abstract classes and provide implementations for a few key methods. These methods will essentially give your cards, pins and connectors their appearance in UI.

Since this is JavaFX based toolkit, all of the visuals are implemented as classes or subclasses of JavaFX visuals, i.e. Pane, Line, TextField, Node, Group, Region etc.

#### Core visual representation

Card, connector and pin all have their visual representations divided into two main parts:

- **root** node
- **main UI** node

where **node** is an elemnt of JavaFX scene graph.

These two nodes can be one and the same node in most of the situations. For more complex of visually appealing implementations, these two nodes refere to different parts of implemented card, pin or connector UI.

*Root node* is the main part used for positioning, adding, removing etc. of cards, pins and connectors in the scene graph.

*Main UI node* is the top-most node in visible UI representation of a card, pin or connector. This is what user sees on the screen. While the root node may be invisible (i.e. `Group`, or even `Pane`, `VBox` etc. but without borders and fill or completely transparent), main UI node is by all means intended to be visible on screen.

#### Core and sample implementations

Base abstract classes that implement core functionalities for each of the above parts are:

- `AbstractDrawingBoard`
- `AbstractCard`
- `AbstractPin`
- `AbstractConnector`

Each of these implements a corresponding API interface:

- `IDrawingBoard` for drawing board implementations
- `ICard` for card implementations
- `IPin` for pin implementations
- `IConnector` for connector implementations

For a quick start, and as a copy-paste base for your own concrete class implementations of these basic abstract classes, CardsFX provides a number of sample implementations for each of them:

- **AbstractDrawingBoard**
    + `SampleDrawingPane`: subclass of JavaFX Pane that contains references to its cards and connectors and provides implementation for creating new default cards and connectors
- **AbstractCard**
    + `CardWithText`: implements card UI as a simple rounded rect with title label, text and 4 types of pins (top, left, bottom and right) for connecting to other cards
- **AbstractPin**
    + `MiniSquarePin`: implements pin UI for a small square pin that can be added to a card top, left, right or bottom edge
- **AbstractConnector**
    + `LineConnector`: connection UI implemented as a straight line between source and target pins
    + `LineConnectorWithArrow`: same as `LineConnector`, with added arrow head at its target pin location
    + `BezierConnector`: connection UI implemented as a cubic Bezier curve between source and target pins
    + `BezierConnectorWithArrow`: same as `BezierConnector`, with added arrow head at its target pin location

This is how sample implementations for card, pin and bezier connector with arrow head looks like, with blue color (#3782B8) selected as a base color:

![][samples]

It does not look pretty or appealing or even precise (you can see the connector is not aligned exactly to the pin center neither on source nor on target). Main purpose for samples is to provide copy-paste type of code base you can use to start building and create beautifully designed graphs and diagrams in your own applications.

#### Drawing Board

Drawing board is a space inside which all the cards, pins and connectors live. You can have multiple drawing boards in your application and each card, pin or connector is owned by only one drawing board.

![][drawing board overview]

Drawing board core functionality is implemented by `AbstractDrawingBoard` class, and concrete implementaion, that can be used in an application, can be found in `SampleDrawingPane` class.

Drawing board main UI node in scene graph (which is either `javafx.scene.layout.Pane` or a subclass of it) will have `drawing-pane` CSS class added to it.

#### Cards

A **card** is UI representation of a graph/diagram node in CardsFX.

Card core functionality is implemented by `AbstractCard `class, and concrete implementation, that can be used in an application, can be found in `CardWithText` class. This is just a sample class and you may want to use it as a starting point for your own class, rather than as an out-of-the box solution.

When implementing a card, at minimum, you'll have to implement these 4 abstract methods from `AbstractCard` class:

```java
    protected abstract Pane createRootUINode();
    protected abstract Node createContentUINode();
    protected abstract void addPinToUI(IPin pin);
    protected abstract void removePinFromUI(IPin pin);
```

Card UI has two main parts:

- root node
- content node (main UI node)

Card **root node** will have `card` CSS style added to it.

Card **content node** will have `card-content` CSS style added to it.

When user selects a card, its **content node** will have `card-selected` CSS style added to it and then removed when the card is deselected.

A card in this image has `StackPane` as its root node, which is marked with dotted line and also has padding set to big enough value so you can see the content node inside of it:

![][a card]

Content node in above image is an instance of `VBox` and it is marked with solid, 2px outline. We placed a title label inside of the content node for this card implementation.

You can also see 4 pins in that image, which will be covered in the next section.

When `AbstractCard` builds UI for the card, it will first call `createRootUINode()` method and only after that it will call `createContentUINode()`. This is done so you can call `getContentNode()` from `createRootUINode()` to be able to add the content UI node to the root UI node scene graph.

Any card's root node has to be a subclass of `javafx.scene.layout.Pane`. The content UI node can be any subclass of `javafx.scene.Node`.

Here is the part of the scene graph from UI displayed on previous image:

![][card overview]

`StackPane` root node is created by code in `createRootUINode()`, which is provided by concrete subclass of `AbstractCard` class. `VBox` content node (main UI node) is created by `createContentUINode()`.

Another implementation of a card can be seen in this image:

![][card overview 2]

It is not particularly pretty or pleasing to the eye, but its exagarated pins and slanted card shape more readily illustrate different elements of the basic concept parts of a card.

`AbstractCard` class also provides support for dragging a card with mouse around a drawing board the card belongs to.

#### Pins

A **pin** is a logical, and often visual, representation of a point on a card to which we can attach one or more connectors.

Pin core functionality is implemented by `AbstractPin` class, and concrete implementation, that can be used in an application, can be found in `MiniSquarePin` class. This is just a sample class and you may want to use it as a starting point for your own class, rather than as an out-of-the box solution.

When implementing a pin, at minimum you'll have to implement these 2 abstract methods from `AbstractPin` class:

```java
protected abstract Region createRootUINode();
protected abstract Node createConnectionPoinUINode();
```

In all the examples here you can usually see 4 pins: top, right, bottom and left one. Some examples also, or only, have center pin positioned at the center of the card. In general case, there can be any number of pins added to a card at any position, depending on your particular card design and intention behind implemented card functionality and behavior.

Here are some pin implementation examples:

![][pin examples]

Pin UI has two main parts:

- root node
- connection point node (usually a main UI node)

Pin **root node** will have `pin` CSS style added to it.

Pin **connection point node** will have `pin-connector` CSS style added to it.

When user hovers mouse over a target pin during drag-and-connect gesture, **connection point node** will have its `:hover` CSS pseudo class activated.

A pin in this image has `HBox` as its root node, which is marked with dotted line and also has padding set so you can see the connection point node inside of it:

![][pin overview]

Connection point node for this pin is an instance of `Rectangle` and is marked with solid, 1px outline and gray fill. When mouse pointer hovers over the connection point UI, in this instance, fill turnes from gray into white to signal to the user they can start or finish drag-and-connect gesture with that pin.

Most of the examples have 4 pins implemented: top, right, bottom and left side pin. You can have as many pins as you need, depending on what your specific implementation of a card represents.

Also, pins in most of the examples are positioned on one of four sides or in the card center. You can place a pin anywhere within, or even outside of the visual card boundaries. It's all up to your design and intention behind it.

When `AbstractPin` builds UI for a pin, it will first call `createConnectionPoinUINode()` method and only after that it will call `createRootUINode()`. This is done in this way because, unless they are the same node, you will want to add connection point UI node to the root node inside of the `createRootUINode()` method.

Any pin's root node has to be a subclass of `javafx.scene.layout.Region`. The connection point UI node can be any subclass of `javafx.scene.Node`.

Here is a part of the scene graph from UI displayed on previous image:

![][pin overview 2]

`HBox` root node for the pin is created by code in `createRootUINode()`, which is provided by concrete subclass of `AbstractPin` class. `Rectangle` connection point node (also a main UI node in this instance) is created in `createConnectionPoinUINode()` method and returned to the method's caller.

`AbstractPin` class also implements drag-and-connect feature enabling user to connect two pins by clicking on a source pin, drag their mouse (click-and-drag), and then release it over target pin.

#### Connectors

A **connector** is visual representation of connection between two cards. In CardsFX, connections are made between specific *pins* which in turn belong to card(s) you want to connect. Every connector connects two pins:

- source (start) and
- target (end) pin

Connector core functionality is implemented by `AbstractConnector` class, and concrete implementation, that can be used in an application, can be found in one of sample implementations: `LineConnector`, `LineConnectorWithArrow`, `BezierConnector` or `BezierConnectorWithArrow`. You can readily use these classes in your application, or they can serve as a base for more flexible and/or functional implementations.

You can see these four sample implementations here, with blue color (#3782B8) selected as a base color:

![][connector sample implementations]

When implementing a connector, at minimum you'll have to implement these 2 abstract methods from `AbstractConnector` class:

```java
protected abstract Node createRootUINode();
protected abstract Node createConnectorShapeUINode();
```

Connector UI has two main parts:

- root node
- connector shape node (in simple cases this is also a main UI node)

Connector **root node** will have `connector` CSS style added to it.

When user selects a connector, its **connector shape node** will have `connector-selected` CSS style added to it and then removed when the connector is deselected.

A connector in this image has UI that consists of only a `javafx.scene.shape.Line`:

![][connector example]

and, in this case, both root node and connector shape node are one and the same.

In more complex connectors, like the one in the next example, root node is usually a `javafx.scene.Group`:

![][connector overview 1]

and connector shape node is whatever shape represents the connection from one card to another, in above case it is `javafx.scene.shape.CubicCurve`.

When `AbstractConnector` builds UI for a connector, it will first call the `createConnectorShapeUINode()` method and only after that it will call `createRootUINode()`. This is done in this way because, unless they are the same node, you will want to add connector shape UI node to the root node inside of the `createRootUINode()` method.

In the case of connector implementations, almost always, you will want to save reference to the node created in `createConnectorShapeUINode()` for later use. This is because, usually, at minimum this node will have to be updated for its position on drawing, everytime a card it connects to is moved.

Here is scene graph for the example from previuos image:

![][connector overview 2]

In this case, connector shape UI node is a simple `javafx.scene.shape.CubicCurve` which is created and returned by `createConnectorShapeUINode()` method in `BezierConnectorWithArrow` implementation. `createRootUINode()` method, in this case, creates a `javafx.scene.Group` as a root UI node for this connector, and adds to it the connector shape UI node created by `createConnectorShapeUINode()`, and also a `javafx.scene.shape.Polygon` which represents an arrow head of the connector.


### Additional core features

Besides providing base implementation of core concepts: drawing board, card, pin and connector, CardsFX also provides a few other functionalities essential to graph/diagram UIs:

- single card selection
- single card positioning by using mouse
- multiple cards selection (group selection)
- multiple cards positioning by using mouse (group positioning)
- single connector selection

These functionalities are implemented through a handful of *manager* and *utility* classes:

- `CardConnectionManager`: provides UI (a dashed gray line, by default) for drag-and-connect gesture and a facility for establishing a connection between two pins
- `CardSelectionManager`: tracks currently selected single card and single connector, tracks a selection of group of cards, provides facility to select multiple cards
- `MouseBoxSelection`: provides UI facility for selecting multiple cards, which is by default a semi-transparent `Rectangle` with thin border. Used by `CardSelectionManager` to determine which cards are selected as a group.
- `CardDraggingContext`: provides functionality of dragging a card with mouse around drawing board




[cardsfx show]: images/01.png "Basic CardsFX pieces"
[samples]: images/samples.png "Samples UI"
[drawing board overview]: images/drawing_board.png "Samples UI"

[a card]: images/card.png "A Card"
[card overview]: images/card_overview.png "Card overview - parts"
[card overview 2]: images/card_overview_2.png "Card overview - example 2"

[pin examples]: images/pin_examples.png "Pin examples"
[pin overview]: images/pin_overview.png "Pin overview - parts"
[pin overview 2]: images/pin_overview_2.png "Pin overview - example 2"

[connector example]: images/connector_example.png "Connector example"
[connector overview 1]: images/connector_overview_1.png "Connector overview - group"
[connector overview 2]: images/connector_overview_2.png "Connector overview - parts"
[connector sample implementations]: images/connector_sample_implementations.png "Sample connector implementations"
