# Technical stuff

For illustration of how to use CardsFX toolkit, let's make a simple application.

You can find the source code for this app in [cardsfx-docexample] repository. The code will build up as we progress through documentation and you can checkout a specific tag from that repository in order to get the application code in a state that corresponds to that specific section in the documentation.

For example:

> checkout `initial_ui` tag to see related code

will tell you the tag to checkout to see how app looks like in that phase.

## Basic application architecture and structure

A couple of words about application architecture and structure

### Architecture

For this documentation, and for all other [CardsFx examples], we will use [builder pattern] and [abstract factory pattern] styles, but we will not implement them strictly.

This is the graph of dependencies that we aim for in this implementation:

![][app structure overview]

### Structure

We'll have basic app launcher class `DocsExampleApp` that extends `javafx.application.Application` and which will create and show main application UI. Application UI will be implemented by `DocsExampleUI` and various actions will then be handed over to `DocsExampleHandler` class which will implement application logic:

- create card
- delete card
- clear drawing board
- delete connector
- select card and connector type for new cards/connectors
- set card title

We will then collect all other code into a sub-package called `deck` (as in 'deck of cards') where we will have sub-packages for cards, connectors and pins.

There will be only a handful of types of cards, pins and connectors:

- cards
  + circle
  + rectangle
- pins
  + transparent pin
- connectors
  + line
  + line with arrow head
  + cubic bezier
  + cubic bezier with arrow head

An instance of each of the elements will be created using a builder class instance specific to that element type:

- cards builder
- connectors builder
- pins builder

Each of the builders will be created by calling a method from another, builder, factory class.

Setting up the application structure in this way makes it almost trivial to add more card, pin and connector types to it. It also makes code management and navigation much easier as application gets more complex.

Ok, with all this out of the way, let's code some graphs.

## Basic app and UI

A class that launches our example application is called `DocsExampleApp` and is very simple:

```java
public class DocsExampleApp extends Application
{
    public static void main( String[] args )
    {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        //---------------------------------------------------------------------
        // title
        primaryStage.setTitle("CardsFX Example from Documentation");

        //---------------------------------------------------------------------
        // UI
        DocsExampleUI ui = new DocsExampleUI();
        Scene scene = new Scene(ui.getRootNode());

        //---------------------------------------------------------------------
        // styles
        scene.getStylesheets().add(Styles.getDefaultCss());
        scene.getStylesheets().add(DocsExampleApp.class.getResource("app.css").toExternalForm());

        //---------------------------------------------------------------------
        // launch
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
```

Building of UI is delegated to `DocsExampleUI` class.

This app launcher code loads two CSS files:

- CardsFX default CSS
- our own application specific CSS: `app.css`

At this point, `app.css` contains just some UI "enhancements":

```CSS
.root {
    -fx-base: #373737;
}

#toolbar Button {
  [omitted ...]
}
#toolbar Button:hover {
  [omitted ...]
}
```

We changed base color for our app, and styled the buttons in toolbar with "dark blue" style from http://fxexperience.com/2011/12/styling-fx-buttons-with-css/.

You can find the code that builds UI in `DocsExampleUI` class, method `createUI()`.

Once you start it, app should look like this:

![][initial app ui]

> checkout `initial_ui` tag to see related code

## Hooking up UI events and application logic

We will wire UI control events in `DocsExampleUI` class and then the event handling, and application logic, will be delegated to `DocsExampleHandler`.

You can see the UI event handling code in `wireEventHandling()` method in `DocsExampleUI` class.

### Application logic

Our app has very simple logic:

- create card
- delete card
- clear drawing board
- delete connector
- select card and connector type for new cards/connectors
- set card title

Any of these are executed immediately and with minimal sanity checking. No alerts, dialog boxes, logs, progress bars etc.

There's a method in `DocsExampleHandler` class for each of the above functionalities. At this point, all of those methods, except for `clearBoard()` are empty. We will implement them in next few sections, which basically show how to use CardsFX toolkit in your app.

> checkout `create_ui_and_handlers` tag to see related code

## Component builders

You can create and manage different types of cards, pins and connectors any way that fits the best to your specific application. For this example, as noted above, we opted to use builder and abstract factory patterns because it is very simple to add more types of components as needed.

The first thing to do is to create `deck` package in the app, with subpackages for cards, pins and connectors:

![][packages]

### Drawing Board

To create anything visual with CardsFX, you'll need a drawing board. The easiest way to create a drawing board is to subclass `com.ravendyne.cardsfx.samples.SampleDrawingPane` and override its `getDefaultsBuilder()` method to return our own implementation of `IDefaultsBuilder` interface.

`IDefaultsBuilder` interface returns a builder instance that will create new cards and connectors. New cards and connectors are created with their properties, whatever they are, set to default values. Application user should then be able to change them, once a card or connector is created and displayed on drawing board.

Our drawing board class will look like this:

```java
public class DocsExampleDrawingPane extends SampleDrawingPane {
    @Override
    public IDefaultsBuilder getDefaultsBuilder() {
        return DocsExampleCreator.getInstance();
    }
}
```

And we will update our `DocsExampleUI` class to use `DocsExampleDrawingPane` to create a drawing board instance. We will create drawing board and add it to the `ScrollPane` in `createUI()` method:

```java
//---------------------------------------------------------------------
// CENTER
drawingScrollPane = new ScrollPane();
drawing = new DocsExampleDrawingPane();
drawingScrollPane.setContent(drawing.getNode());
```

`IDrawingBoard` interface has `getNode()` method and the implementation should return JavaFX `Node` instance (in our case `Pane`) that represents the drawing board in the scene.

UI should look something like this now:

![][app with drawing board]

Drawing board, by default, has an orange border and textured background. We will change this later on.

### Abstract factory for component builders

Notice that `getDefaultsBuilder()` calls `getInstance()` from a class `DocsExampleCreator`. This class implements abstract factory pattern (not implemented strictly by the book, but close enough for what we need) where there are a few methods that return builders for pins, cards and connectors which you then use to create those components.

We'll not be following Java abstract factory pattern conventions here, we'll just borrow the concept and implement it the way it fits best to what we are doing in our app. That being said, our `DocsExampleCreator` will look something like this:

```java
public class DocsExampleCreator implements IDefaultsBuilder {
    private static DocsExampleCreator instance;

    private DocsExampleCreator() {}

    public static DocsExampleCreator getInstance() {
        if(instance == null) {
            instance = new DocsExampleCreator();
        }

        return instance;
    }

    public DocsExamplePinBuilder pin() {
        return new DocsExamplePinBuilder();
    }

    public DocsExampleCardBuilder card() {
        return new DocsExampleCardBuilder();
    }

    public DocsExampleConnectorBuilder connector() {
        return new DocsExampleConnectorBuilder();
    }

    @Override
    public ICard newDefaultCard(IDrawingBoard drawing) {
        return null;
    }

    @Override
    public IConnector newDefaultConnector(IDrawingBoard drawing, IPin source, IPin target) {
        return null;
    }

}
```

We will implement the methods that return default card and connector later, as well as add simple facilities for user to select which card and connector types they would like to be used as defaults.

> checkout `basic_factory_and_dummy_builders` tag to see related code

At this point, source tree should look something like this:

![][source structure 01]

Card, connector and pin builder classes are empty classes that do nothing at this point and are there so we can compile the code without errors and run it to see how UI looks like.

### Components

In order to create a card we would also need to create one or more pins for it, otherwise the card can't be connected to any other one, so we'll first start with pins.

#### Pins

For this example, we will create one type of pin. The pin will be invisible on screen except when mouse pointer goes over it. On mouse over, the pin will be filled with semi-transparent white fill to indicate that user can start or finish a drag-and-connect gesture on that pin.

##### Location

One of important pin attributes is its **location**.

There are 5 locations on a card where you can add pins:

- top
- left
- bottom
- right
- center

These location have more to do with how connectors connect to pins than they have with physical locations of pins on a card. In general, you can place your pins anywhere on (or even outside of) a card where appropriate for your specific application. Specifying that a pin is in one of those 5 locations actually gives a hint to a connector which connects to it. This hint mostly helps in determining how to render or position connector end that is connected to that specific pin.

For example, a type of connector represente by a cubic Bezier curve will use pin's location to determine where to position its Bezier curve control points.

Depending on specific implementation, a pin may support being added to all or only some locations (i.e. only `center` location). Also, a connector may support connecting only to pins on certain locations (i.e. our implementation of cubic Bezier connector type doesn't support connecting to a pin in `center` location).

Our transparent pin will support being added to all 5 locations.

##### Connector attachment point on a pin

Besides it's location, another important pin attribute is a **connector attachment point**.

This is a point on the pin's *connection point UI node* where a connector connected to that pin should position its end. For example, if a pin has `javafx.scene.shape.Rectangle` as its UI node, then you may define the connector attachment point for that pin type to be the center of the rectangle.

For our transparent pin type we will define two attachment points:

- one for when the pin is in top, left, bottom or rigth location
- one for when the pin is in center location

##### Transparent pin implementation

We will name our transparent pin class, you've probably guessed it by now, `DocsExampleTransparentPin`. This class extends the `AbstractPin` class and at minimum has to implement following methods:

- `createRootUINode()`
- `createConnectionPoinUINode()`
- `getAttachmentPositionLocal()`

Since our pin is essentially invisible until the moment user hovers their mouse over it, we will use `javafx.scene.layout.Pane` for pin's UI node. We will adjust size of this Pane node based on the pin's *location*. We will also adjust connector attachment point position inside this Pane based on the pin's *location*.

Minimal implementation for a pin (and a starting point for our transparent pin) would look something like this:

```java
public class DocsExampleTransparentPin extends AbstractPin {
    Pane pinUI;

    DocsExampleTransparentPin(ICard card) {
        super(card);
    }

    @Override
    protected Region createRootUINode() {
        return pinUI;
    }

    @Override
    protected Node createConnectionPoinUINode() {
        pinUI = new Pane();
        return pinUI;
    }

    @Override
    public Point2D getAttachmentPositionLocal() {
        return new Point2D(
            getConnectorAttachmentNode().getLayoutBounds().getWidth(),
            getConnectorAttachmentNode().getLayoutBounds().getHeight()
        );
    }
}
```

In this implementation we are relying on the fact that `AbstractPin` will **always** call `createConnectionPoinUINode()` **first** and `createRootUINode()` **second** in the process of building pin's UI.

If you were using defensive programming style, you might have implemented those two methods differently, for example like this:

```java
private Pane getPinUI() {
    if(pinUI == null) {
        pinUI = new Pane();
    }

    return pinUI;
}

@Override
protected Region createRootUINode() {
    return getPinUI();
}

@Override
protected Node createConnectionPoinUINode() {
    return getPinUI();
}
```

Since we are coding this exaple for illustration, we will stick to the simpler approach.

Next we need to implement few more things:

- we have to set dimensions for our pin based on its location
- we have to correctly set connector attachment point based on pin's location

To do this, we will override `setLocation(Location)` method. `Location` enumeration is defined in `IPin` interface. Our `setLocation(Location)` method looks like this:

```java
@Override
public void setLocation(Location location) {
    super.setLocation(location);
    setDimensions();
    setHotspotData();
}
```

`setDimensions()` will size the Pane node we use for UI like this:

- for **top** and **bottom** locations:
    + set Pane height to 8 pixels and make its width managed by its parent
- for **left** and **right** locations:
    + set Pane width to 8 pixels and make its height managed by its parent
- for **center** location:
    + set Pane height and width to 8 pixels

Our `setDimensions()` method will then look like this:

```java
public static final double connectorSize = 8;

private void setDimensions() {
    switch(getLocation()) {
    case Top:
    case Bottom:
        pinUI.setMaxHeight(connectorSize);
        pinUI.setMaxWidth(-1);
        break;
    case Left:
    case Right:
        pinUI.setMaxHeight(-1);
        pinUI.setMaxWidth(connectorSize);
        break;
    case Center:
        pinUI.setMaxHeight(connectorSize);
        pinUI.setMaxWidth(connectorSize);
        break;
    }
}
```

where we use `connectorSize` constant instead of fixed 8px value, so we can easily change it later, if we need to. Instead of calling `setMax**()` methods, you could call `setPref**()` ones or maybe use some other means of constraining this Pane's dimenstions. In the end it's all up to what you need to achieve and what works in your specific case.

Next, for connector attachment point we want to achieve this:

- if a pin's location is `center` we want attachment point to be in the Pane center
- if a pin's location is one of `top`, `right`, `bottom` or `left` we want attachment point to be on the top, right, bottom or left Pane edge, respectively. We also want it to be centered on that edge.

Since the location of the attachment point is in pin's local coordinates, in our case relative to top-left corner of the Pane node, one of the ways to specify the point position in general terms would be to use relative coordinates.

For that purpose we will designate top-left corner of our Pane as a point with (0,0) coordinates, and bottom-right corner point as having coordinates (1,1). Then, specifying attachment point location would come down to this:

- **center** location: ( 0.5 , 0.5 )
- **top** location: ( 0.5 , 0.0 )
- **bottom** location: ( 0.5 , 1.0 )
- **left** location: ( 0.0 , 0.5 )
- **right** location: ( 1.0 , 0.5 )

That being said, our `setHotspotData()` method will then look like this:

```java
private double hotspotXParam;
private double hotspotYParam;

private void setHotspotData() {
    switch(getLocation()) {
    case Top:
        hotspotXParam = 0.5;
        hotspotYParam = 0.0;
        break;
    case Bottom:
        hotspotXParam = 0.5;
        hotspotYParam = 1.0;
        break;
    case Left:
        hotspotXParam = 0.0;
        hotspotYParam = 0.5;
        break;
    case Right:
        hotspotXParam = 1.0;
        hotspotYParam = 0.5;
        break;
    case Center:
        hotspotXParam = 0.5;
        hotspotYParam = 0.5;
        break;
    }
}
```

We also need to update `getAttachmentPositionLocal()` method to take our `hotspotX/YParam` values:

```java
@Override
public Point2D getAttachmentPositionLocal() {
    return new Point2D(
        hotspotXParam * getConnectorAttachmentNode().getLayoutBounds().getWidth(),
        hotspotYParam * getConnectorAttachmentNode().getLayoutBounds().getHeight()
    );
}
```

You may have noticed that `DocsExampleTransparentPin` constructor has Java default, **package**, visibility. This is on purpose, since we want the constructor to be accessible only to the pin builder. For this reason, we have placed the pin builder in the `pins` package, same as `DocsExampleTransparentPin` class. We'll follow the same recipe for cards and connectors too.

And that's it.

##### Pin builder implementation

Now that we have our one pin type implemented, we'll implement the `DocsExamplePinBuilder` class, which has been empty until now.

In this case, implementation is straightforward and simple:

```java
public class DocsExamplePinBuilder {

    ICard pCard;
    Location pLocation;

    public DocsExamplePinBuilder() {
    }

    public DocsExamplePinBuilder setCard(ICard card) {
        Objects.requireNonNull(card);
        pCard = card;
        return this;
    }

    public DocsExamplePinBuilder setLocation(Location location) {
        Objects.requireNonNull(location);
        pLocation = location;
        return this;
    }

    public IPin build() {
        Objects.requireNonNull(card);
        Objects.requireNonNull(location);

        IPin pin = new DocsExampleTransparentPin(pCard);
        pin.buildUI();
        pin.setLocation(pLocation);
        pCard.addPin(pin);

        return pin;
    }

}
```

In order to create a pin, the builder needs a card to attach pin to and it needs the pin's location, so we have two builder methods here: `setCard(ICard)` and `setLocation(Location)`.

And, as builder pattern goes, if for example, you had more pin types and for some of them you could set their color or shape or whatnot, you would add methods to the builder for setting those properties.

Finally, the builder's `build()` method will create our pin instance with given parameters. The build includes creating the pin instance, setting its location and then adding it to the specified card.

Elsewhere in your code, you would then create pins like this:

```java
    IPin pin = DocsExampleCreator.getInstance()
        .pin()
        .setCard(aCard)
        .setLocation(Location.Top)
        .build();
```

The builder pattern seems like an overkill for our simple case here, and for this simple case it may very well be. It does however shines once you start doing things like:

**requirement**: we need to have multiple drawing boards and each will have its own set of cards, pins etc.

**solution**: let `DocsExampleCreator` return builder factory instance specific to each board

After you make that change, all call sites in your code that create new pins will change to:

```java
    IPin pin = DocsExampleCreator.getInstance(aDrawingInstance)
        .pin()
        .setCard(aCard)
        .setLocation(Location.Top)
        .build();
```

Easy-peasy. :)

We'll also see how using builder pattern helps minimize work needed to add more types of components, in our case cards and connectors, which is covered in the next section.

> checkout `pin_implementation` tag to see the pin implementation code

#### Cards

For this example, we will create two types of cards:

- rectangle card
- circle card

Rectangle card will have 4 pin locations: top, bottom, left and right, and will accept only one pin for each location.

Circle card will have one pin location: center, and will accept only one pin for that location.

The only pin that will be added to each of these card types is our transparent pin we implemented in the previous section of this documentation.

##### Circle card implementation

Our circle card will have a UI node that is, well, an instance of `javafx.scene.shape.Circle`, and will accept one pin at its center.

If you looked at it, you may have noticed that `ICard` API does not say anything specific about card content, other than that it should have a **content node**. Since a card content highly depends on its intended use and what it represents in your application, API does not impose any constraints in that regards.

The same is true for `IPin` and `IConnector` APIs.

In relation to this, just for convenience, CardsFX does provide two additional interfaces which you may or may not use:

- `ITitledCard`
- `ILabeledConnector`

These interfaces are optional and are guaranteed not to be referenced by any code in CardsFX toolkit other than classes in `samples` package.

Both of those are very simple and are used throughout [CardsFx examples] project. `ITitledCard` defines getter and setter intended for use to get/set card title, and `ILabeledConnector` defines getter and setter intended for use to get/set connector label.

We will implement `ITtitledCard` interface for our rectangle and circle cards.

That being said, here is how minimum implementation of circle card, in the class called `DocsExampleCircleCard`, looks like:

```java
public class DocsExampleCircleCard extends AbstractCard implements ITitledCard {

    Label titleLabel;

    DocsExampleCircleCard(IDrawingBoard drawing, String title) {
        super(drawing, title);

        titleLabel = new Label( title );
    }

    @Override
    protected Pane createRootUINode() {
        StackPane cardPane = new StackPane();

        titleLabel.setTranslateY(-20);

        cardPane.getChildren().add(getContentNode());
        cardPane.getChildren().add(titleLabel);

        cardPane.getStyleClass().add("circle");

        return cardPane;
    }

    @Override
    protected Node createContentUINode() {
        Circle uiCircle = new Circle();

        uiCircle.setRadius(DocsExampleTransparentPin.connectorSize + 2);

        return uiCircle;
    }

    @Override
    protected void removePinFromUI( IPin pin ) {
    }

    @Override
    protected void addPinToUI(IPin pin) {
    }

    @Override
    public String getTitle() {
        return titleLabel.getText();
    }

    @Override
    public void setTitle(String text) {
        titleLabel.setText(text);
    }

}
```

When implementing you own card type, you extend `AbstractCard` class and at minimum you have to implement 4 methods: two to create UI nodes and two to add/remove a pin to/from the card.

In above code we only implemented UI part, and we'll handle the pin add/remove part in the following paragraphs.

UI for circle card consists of:

- `javafx.scene.layout.StackPane` as a **root node**
- `javafx.scene.shape.Circle` as a **main UI mode**

We also have `javafx.scene.control.Label` as a part of scene graph for this card type. The label represents circle card type's title. We add the label to the root node (StackPane) and set its `translateyProperty()` to `-20`. This will effectively position our card's title above the Circle shape. The title will be centered on the card since `StackPane` centers its children by default.

Notice three things we did when implementing UI part:

- in `createRootUINode()` we call `getContentNode()` method and add the `Node` it returns to our card's `StackPane`. This method returns the `Node` created by `createContentUINode()`
- in `createContentUINode()`, we set `Circle` radius to `DocsExampleTransparentPin.connectorSize + 2`. This effectively ties our circle card implementation to the `DocsExampleTransparentPin` and this pattern should be avoided in general. Better place to put `connectorSize` might be `DocsExamplePinBuilder` class. For simplicity sake, we leave the code as is now.
- we added CSS style `circle` to the root node. This way we can target this specific card type with CSS styling.

Now we need to implement methods to add/remove pin to the card. These methods are called by `AbstractCard` code when a pin's UI needs to be added to the card UI. `AbstractCard` doesn't know (and doesn't need to know) how exactly scene graph sub-tree for your card looks like, so it delegates adding and removing a pin UI to your code.

That being said, here is how the code to add/remove pin to our circle card UI looks like:

```java
IPin centerPin;

@Override
protected void removePinFromUI( IPin pin ) {
    getRootNode().getChildren().remove( pin.getRootNode() );
}

@Override
protected void addPinToUI(IPin pin) {
    switch(pin.getLocation()) {
    case Top:
    case Bottom:
    case Left:
    case Right:
        throw new UnsupportedOperationException("Can only add pin to card center.");
    case Center:
        if(centerPin != null) {
            throw new UnsupportedOperationException("Can't add pin to already populated location");
        }

        centerPin = pin;
        StackPane.setAlignment(pin.getRootNode(), Pos.CENTER);

        break;
    }

    getRootNode().getChildren().add( pin.getRootNode() );
}
```

First, in this case, we are keeping a reference to the pin that was added to our circle card. We do this so because we want to support adding **only one** pin to our card and adding it **only once**. We might have skipped the **only once** part, in which case we would call `getRootNode().getChildren().remove( centerPin )` instead of throwing the `UnsupportedOperationException`.

Second thing is that we are adding the pin to the **root** node of our UI, which is the `StackPane` created by `createRootUINode()` method. In most cases, you would add your pins as a child, or descendent of, the node created by `createContentUINode()`. We will do that in the rectangle card type implementation.

For the circle card type, first, we can't add child nodes to `javafx.scene.shape.Circle` and second, adding the `center` pin to our `StackPane` root node works perfect, since it will be centerd on all `StackPane` content, just as we need it to be.

And as wrap-up for our circle card, we will add override of `discard()` method. Usually, you don't need to override this method in simple cases. One case when you might want to do it is when you are trying to hunt down memory leaks in your application. to illustrate this, we add `discard()` method to our implementation:

```java
@Override
public void discard() {
    super.discard();

    // since we added references to these two to our root UI node
    // it helps with memory leak debugging to nullify it here
    titleLabel = null;
    centerPin = null;
}
```

One source of memory leaks would result from multiple cycles of adding/removing components to a drawing board, i.e. when calling `IDrawingBoard.clean()` or when disposing current drawing board content to replace it with different one loaded from a file, etc.

The `dispose` method is provided to help release object references and after garbage collection is done you can easily identify where are the un-released references that cause memory leaks. Memory leaks may become an issue for bigger graphs and long-running appliations.

Next, we'll implement simple rectangle type card.

##### Rectangle card implementation

For our rectangle type card, we will also have `StackPane` as a root node. For main UI node we will use `javafx.scene.layout.VBox` and will add card's title label to the `VBox`.

We could have used the root node as main UI node too in this case and add the title label to it. Just for the sake of illustration, we'll not do that.

UI implementation of rectangle card looks like this:

```java
public class DocsExampleRectangleCard extends AbstractCard implements ITitledCard {

    Label titleLabel;

    DocsExampleRectangleCard(IDrawingBoard drawing, String title) {
        super(drawing, title);

        titleLabel = new Label( title );
    }

    @Override
    protected Pane createRootUINode() {
        StackPane cardPane = new StackPane();

        cardPane.getChildren().add(getContentNode());

        cardPane.getStyleClass().add("rectangle");

        return cardPane;
    }

    @Override
    protected Node createContentUINode() {
        VBox content = new VBox();
        content.setAlignment(Pos.CENTER);

        content.getChildren().add(titleLabel);
        return content;
    }

    @Override
    protected void removePinFromUI( IPin pin ) {
    }

    @Override
    protected void addPinToUI(IPin pin) {
    }

    @Override
    public String getTitle() {
        return titleLabel.getText();
    }

    @Override
    public void setTitle(String text) {
        titleLabel.setText(text);
    }

}
```

Just like with the circle type card, we also added CSS style `rectangle` to the root node. We'll use this later on to tweak this card type appearance a bit.

`addPinToUI(IPin)` method override in this case is similar to the circle type one, just a bit more involved since we have 4 pin locations instead of one:

```java
IPin topPin;
IPin bottomPin;
IPin leftPin;
IPin rightPin;

@Override
protected void removePinFromUI( IPin pin ) {
    getRootNode().getChildren().remove( pin.getRootNode() );
}

@Override
protected void addPinToUI(IPin pin) {
    IPin oldPin = null;

    switch(pin.getLocation()) {
    case Top:
        oldPin = topPin;
        topPin = pin;
        break;
    case Bottom:
        oldPin = bottomPin;
        bottomPin = pin;
        break;
    case Left:
        oldPin = leftPin;
        leftPin = pin;
        break;
    case Right:
        oldPin = rightPin;
        rightPin = pin;
        break;
    case Center:
        throw new UnsupportedOperationException("Cant't add pin to card center.");
    }

    if(oldPin != null) {
        throw new UnsupportedOperationException("Can't add pin to already populated location");
    }

    setPinLocation(pin);

    getRootNode().getChildren().add( pin.getRootNode() );
}

private void setPinLocation(IPin pin) {
    switch (pin.getLocation()) {
    case Top:
        StackPane.setAlignment(pin.getRootNode(), Pos.TOP_CENTER);
        break;
    case Right:
        StackPane.setAlignment(pin.getRootNode(), Pos.CENTER_RIGHT);
        break;
    case Bottom:
        StackPane.setAlignment(pin.getRootNode(), Pos.BOTTOM_CENTER);
        break;
    case Left:
        StackPane.setAlignment(pin.getRootNode(), Pos.CENTER_LEFT);
        break;
    case Center:
        throw new UnsupportedOperationException("Cant't set pin location to card center.");
    }
}
```

We added private `setPinLocation(IPin)` method since we want to adjust pin location attribute only after it has been accepted as a part of this card.

Other than that, the implementation is pretty much the same as in circle card, except this time we have 4 locations. Also, we use `oldPin` variable here in order not to repeat throwing same `UnsupportedOperationException` 4 times.

##### Card builder implementation

Now that we have our two card types implemented, let's implement the `DocsExampleCardBuilder` class. Once we do that, we can run the app to see how our new cards look like.

For our cards builder, we will need to implement at minimum setters for a drawing that card belongs to and for the card's title.

```java
public class DocsExampleCardBuilder {

    protected IDrawingBoard pDrawing;
    protected String pTitle;

    public DocsExampleCardBuilder() {
        pCardType = DocsCardType.Rectangle;
    }

    public DocsExampleCardBuilder setDrawing(IDrawingBoard drawing) {
        Objects.requireNonNull(drawing);
        pDrawing = drawing;
        return this;
    }

    public DocsExampleCardBuilder setTitle(String title) {
        Objects.requireNonNull(title);
        pTitle = title;
        return this;
    }

}
```

Since we have two different card types, we'll also add a setter for the card type we wish to build:

```java
    public static enum DocsCardType {
        Circle,
        Rectangle
    }

    DocsCardType pCardType;

    public DocsExampleCardBuilder setType(DocsCardType type) {
        Objects.requireNonNull(type);
        pCardType = type;
        return this;
    }
```

We also added here `DocsCardType` enum which our app will use to specify the card type it wishes the builder to create.

And, last but not least, we implement the `build()` method that does the job,

```java
    public ITitledCard build() {
        Objects.requireNonNull(pDrawing);
        Objects.requireNonNull(pTitle);

        ITitledCard card = null;
        switch(pCardType) {
        case Circle:
            card = new DocsExampleCircleCard( pDrawing, pTitle );
            break;
        case Rectangle:
            card = new DocsExampleRectangleCard( pDrawing, pTitle );
            break;
        }
        card.buildUI();
        pDrawing.addCard(card);

        return card;
    }
```

`build()` creates a card of specified type and then adds it to the specified drawing.

In our application, when we want to create a new card, the code would look something like this:

```java
DocsExampleCreator.getInstance()
    .card()
    .setType(DocsCardType.Circle)
    // or
    // .setType(DocsCardType.Rectangle)
    .setDrawing(aDrawingInstance)
    .setTitle("the card title")
    .build()
```

Now, let's add a temporary code that will create one rectangle and one circle card just so we can see them.

We will add this code to the `DocsExampleHandler` class, in `newCard()` method:

```java
ICard circleCard = DocsExampleCreator.getInstance()
.card()
.setType(DocsCardType.Circle)
.setDrawing(drawing)
.setTitle("circle card")
.build();

ICard rectangleCard = DocsExampleCreator.getInstance()
.card()
.setType(DocsCardType.Rectangle)
.setDrawing(drawing)
.setTitle("rectangle card")
.build();

circleCard.setPosition(new Point2D(50, 50));
rectangleCard.setPosition(new Point2D(150, 150));
```

Don't forget to update the `DocsExampleHandler` constructor to save drawing board reference:

```java
public DocsExampleHandler(IDrawingBoard drawing) {
    this.drawing = drawing;
}
```

Once this is done, you can run the app and when you click on "New card" button you should see something like this:

![][two cards]

You should be able to click-and-drag the cards around the drawing board. Also, when you click on either circle or rectangle, it should indicate that it's been selected by different border:

![][selectd card]

If you click on the drawing board and drag your mouse you'll notice mouse selection box:

![][mouse selection]

If you select both cards as in the picture above, you can then click and drag either of them and they will both be moved together as you move your mouse around.

All these functionalities come out of the box.

> checkout `two_cards` tag to see the new code that implements the two card types


#### Connectors

CardsFX comes with a set of four connector type samples: line, line with arrow, cubic bezier and cubic bezier with arrow. While we could use these out-of-the box, for the sake of illustration on what steps you need to do to implement your own, we will re-implement two connector types: line and cubic bezier with arrow.

##### Line connector implementation

Line connector is the simplest one to implement: we will use `javafx.scene.shape.Line` to represent a connection between two pins on two cards.

Our line connector will then have root node the same as it's main UI node: an instance of `javafx.scene.shape.Line` class.

You implement a connector by extending `AbstractConnector` class. Minimal implementation has to implement three methods: `createRootUINode()`, `createConnectorShapeUINode()` and `updatePosition()`. Since we use the same node as both root and connector shape node, we will create our line shape in `createConnectorShapeUINode()` and just return the reference from `createRootUINode()`.

Our `DocsExampleLineConnector` class will then look like this:

```java
public class DocsExampleLineConnector extends AbstractConnector {
    private Line theLine;

    DocsExampleLineConnector(IDrawingBoard drawing, IPin sourcePin, IPin targetPin) {
        super(drawing, sourcePin, targetPin);
    }

    @Override
    protected Node createRootUINode() {
        return theLine;
    }

    @Override
    protected Node createConnectorShapeUINode() {
        theLine = new Line();

        return theLine;
    }

    @Override
    public void updatePosition() {
    }
}
```

Notice how connector constructor accepts three parameters: drawing board and source and target pins.

While pins are added to, and mostly managed by, a card, cards and connectors are added to the drawing board. That's why we need a reference to a drawing board we want this connector to belong to and to be displayed at.

With UI done (that was easy), `updatePosition()` method is also fairly straight forward, since we are dealing with a simple line:

```java
@Override
public void updatePosition() {
    Point2D sourceConnectionPointOnDrawing = source.getAttachmentPosition();
    Point2D targetConnectionPointOnDrawing = target.getAttachmentPosition();

    final double startX = sourceConnectionPointOnDrawing.getX();
    final double startY = sourceConnectionPointOnDrawing.getY();
    final double endX = targetConnectionPointOnDrawing.getX();
    final double endY = targetConnectionPointOnDrawing.getY();

    theLine.setStartX(startX);
    theLine.setStartY(startY);
    theLine.setEndX(endX);
    theLine.setEndY(endY);
}
```

Notice how you get connector's start and end reference points by calling `getAttachmentPosition()` method on source and target pins respectively. The code above might seem a bit too verbose, we did it that way to emphasize few points, the main one bring that `getAttachmentPosition()` returns *attachment point position in drawing board coordinate space*. Since connectors are added to a drawing board, you can use these coordinates directly, without any transformations, which is what we have done.

Next, we will implement connector builder and after that we will implement one step that was omitted from the card implementation section: adding pins to the cards!

Once this is done, we can start the application to try how connectors work.

##### Connectors builder

Here we implement only enough code in `DocsExampleConnectorBuilder` to support our currently only connector type: simple line.

To create a connector we will need to implement at least 3 setters: to set drawing board, source pin and target pin. Let's do that:

```java
public class DocsExampleConnectorBuilder {

    private IDrawingBoard pDrawing;
    private IPin pSource;
    private IPin pTarget;

    public DocsExampleConnectorBuilder setDrawingBoard(IDrawingBoard drawing) {
        Objects.requireNonNull(drawing);
        pDrawing = drawing;
        return this;
    }

    public DocsExampleConnectorBuilder setSource(IPin sourcePin) {
        Objects.requireNonNull(sourcePin);
        pSource = sourcePin;
        return this;
    }

    public DocsExampleConnectorBuilder setTarget(IPin targetPin) {
        Objects.requireNonNull(targetPin);
        pTarget = targetPin;
        return this;
    }

    public IConnector build() {
        Objects.requireNonNull(pDrawing);
        Objects.requireNonNull(pSource);
        Objects.requireNonNull(pTarget);

        IConnector connector = new DocsExampleLineConnector(pDrawing, pSource, pTarget);

        connector.buildUI();
        pDrawing.addConnector(connector);

        return connector;
    }

}
```

Once the required parameters are set, the `build()` method is straightforward:

```java
public IConnector build() {
    Objects.requireNonNull(pDrawing);
    Objects.requireNonNull(pSource);
    Objects.requireNonNull(pTarget);

    IConnector connector = new DocsExampleLineConnector(pDrawing, pSource, pTarget);

    connector.buildUI();
    pDrawing.addConnector(connector);

    return connector;
}
```

We create the connector first, call its `buildUI()` method and finally add it to the intended drawing board.

##### Adding pins to cards and defaults builder

When user creates a card or connects two cards with a connector, the application usually creates whatever is currently a default type of card or connector.

CardsFX API for creating default cards and connectors is defined by `IDefaultsBuilder` interface. In addition to that, `IDrawingBoard` API contains method `getDefaultsBuilder()` which should return defaults builder implementation for the drawing board. There is intentional ambiguity built into these specifications: `IDrawingBoard.getDefaultsBuilder()` returns an implementation of `IDefaultsBuilder` and then when you call any method on that defaults builder instance, you still have to pass a drawing board as an argument! This is done so that you can either tie defaults builder(s) to a specific drawing board (if you have more than one in your application), or you can implement `IDefaultsBuilder` independently of any drawing board and have `IDrawingBoard.getDefaultsBuilder()` throw `UnsupportedOperationException`.

You may have noticed that for this documentation's example application, we have taken another (possible) approach: `DocsExampleCreator` class implements `IDefaultsBuilder` and our `DocsExampleDrawingPane.getDefaultsBuilder()` returns the (singleton) instance of `DocsExampleCreator`. This simply means we have only one `IDefaultsBuilder` implementation in our application.

That being said, here is how the implementation of `IDefaultsBuilder` in `DocsExampleCreator` looks like now:

```java
private DocsCardType defaultCardType = DocsCardType.Circle;

public void setDefaultCardType(DocsCardType type) {
    defaultCardType = type;
}

@Override
public ICard newDefaultCard(IDrawingBoard drawing) {
    ICard newCard = DocsExampleCreator.getInstance()
    .card()
    .setType(defaultCardType)
    .setDrawing(drawing)
    .setTitle("a card")
    .build();

    return newCard;
}

@Override
public IConnector newDefaultConnector(IDrawingBoard drawing, IPin source, IPin target) {
    IConnector connector = connector()
            .setDrawingBoard(drawing)
            .setSource(source)
            .setTarget(target)
            .build();
    return connector;
}
```

Since we have two card types to choose from, we added `defaultCardType` attribute to `DocsExampleCreator` and just set it to `DocsCardType.Circle` for now. We will make it changeable by user later on.

With this code in place, we can update the `DocsExampleHandler.newCard()` method like so:

```java
public void newCard() {
      DocsExampleCreator.getInstance().setDefaultCardType(DocsCardType.Circle);
      ICard circleCard = DocsExampleCreator.getInstance().newDefaultCard(drawing);

      DocsExampleCreator.getInstance().setDefaultCardType(DocsCardType.Rectangle);
      ICard rectangleCard = DocsExampleCreator.getInstance().newDefaultCard(drawing);

      circleCard.setPosition(new Point2D(50, 50));
      rectangleCard.setPosition(new Point2D(150, 150));
}
```

Lastly, before our `IDefaultsBuilder` returns an instance of a card, we want to populate the card with all the pins the card can have: 1 for `circle` and 4 for `rectangle`. We add this logic to `newDefaultCard` method:

```java
switch(defaultCardType) {
case Circle:
    pin().setCard(newCard).setLocation(Location.Center).build();
    break;
case Rectangle:
    pin().setCard(newCard).setLocation(Location.Top).build();
    pin().setCard(newCard).setLocation(Location.Bottom).build();
    pin().setCard(newCard).setLocation(Location.Left).build();
    pin().setCard(newCard).setLocation(Location.Right).build();
    break;
}
```

If you run application at this point and click the "New card" button, you'll get something like this:

![][cards with pins version]

Both card have the same (default) title now, and if you try to click on the rectangle card and move it wit mouse, you'll find that you really need to hit the center of it to do so. This is because all four pins are added to the card now and, since the card is fairly small, they cover most of it.

Also, when you move your mouse over a pin, you don't really know that the mouse pointer is over the pin, since is our `DocsExampleTransparentPin` is, well, transparent.

With a help of some CSS styling we will get around this. After adding these lines to `app.css`:

```css
.rectangle {
    -fx-min-height: 40;
    -fx-min-width: 80;
}

.card-content {
    -fx-padding: 5;
}

.pin-connector:hover {
    -fx-background-color: rgba(255,255,255,0.8);
}
```

and re-running the application, you will notice that now:

- rectangle card is bigger
- when hovering over a pin (center for `circle` and any of the four sided for `rectangle`) the pin is highlighted with semi-transparent white

![][added css for pins and card]

It is now a lot easier to click on a pin, drag the mouse, and release it on another pin which will create a connection (line in our case) between two pins (and cards, if pins are on different cards):

![][connected cards]

Ta-daaa!

With few more tweaks, and added cubic Bezier connector for variety, you will have created a simple diagramming JavaFX application!


> checkout `connected_cards` tag to see the code with line connector and defaults builder implementation


##### Cubic Bezier connector implementation

We'll now create cubic Bezier connector implemented in `DocsExampleBezierConnector` class.

This connector is a bit more involved because it:

- uses cubic Bezier curve as connector shape
- displays an arrow head shape at the target pin end

We will implement the connector shape using `javafx.scene.shape.CubicCurve` and the arrow head by using `javafx.scene.shape.Polygon`.

As usual, we will subclass the `AbstractConnector` class, and that requires us to implement `createRootUINode()` and `createConnectorShapeUINode()` methods, each returning a `javafx.scene.Node`. The root UI node must be exactly that: a root node of the JavaFX scene sub-graph representing our connector. Since `javafx.scene.shape.Shape` can only be added as a leaf node to JavaFX scene, and we have two shapes for our connector, we will use `javafx.scene.Group` as a root UI node:

```java
public class DocsExampleBezierConnector extends AbstractConnector {
    CubicCurve connectorCurve;
    Polygon arrowHead;

    DocsExampleBezierConnector(IDrawingBoard drawing, IPin sourcePin, IPin targetPin) {
        super(drawing, sourcePin, targetPin);
    }

    @Override
    protected Node createRootUINode() {
        Group connectorGroup = new Group();

        arrowHead = new Polygon();
        arrowHead.getPoints().addAll(new Double[] {0.0, 0.0, 10.0, 0.0, 5.0, 20.0});
        arrowHead.getStyleClass().add(Styles.CONNECTOR_ARROW_HEAD_STYLE);
        arrowHead.setStrokeType(StrokeType.INSIDE);

        connectorGroup.getChildren().add(connectorCurve);
        connectorGroup.getChildren().add(arrowHead);

        precalc();

        return connectorGroup;
    }

    @Override
    protected Node createConnectorShapeUINode() {
        connectorCurve = new CubicCurve();

        connectorCurve.setFill(null);

        return connectorCurve;
    }

    @Override
    public void updatePosition() {
    }
}
```

We choose the `CubicCurve` instance to be our connector shape UI node, since we can only set one `Shape` for that role. Whatever you return as your connector shape UI node, that will be the shape that user needs to click in order to select this specific connector.

Of course, you can return any `javafx.scene.Node` as connector shape UI node, just keep in mind that in that case handling mouse clicks on it and updating its position on drawing board is a bit more involved. The simplest way is to just return one of the shapes (if there are more than one) as the connector shape UI node, and most often that would be the connector curve (or line, or polyline etc.) itself.

Implementation of the`updatePosition()` method for cubic Bezier curve with arrow head is a bit more involved than the one for simple straight line connector and it won't be covered here. Our aim is to explain usage of CardsFX toolkit and show examples of how to implement its API. You can see the implementation of `updatePosition()` in the [cardsfx-docexample] repository code.

Now that we have our second type of connector, let's add it to the `DocsExampleConnectorBuilder` class and try it out!

We amend `DocsExampleConnectorBuilder` builder class with `ConnectorType` enumeration and add setter for connector type parameter to be used when the builder creates new connectors. We also update the `build()` method to create a curved connector when that is the selected as current connector type:

```java
public static enum ConnectorType {
    Line,
    Curve,
}

private ConnectorType pConnectorType;

public DocsExampleConnectorBuilder setType(ConnectorType connectorType) {
    Objects.requireNonNull(connectorType);
    pConnectorType = connectorType;
    return this;
}

public IConnector build() {
    Objects.requireNonNull(pDrawing);
    Objects.requireNonNull(pSource);
    Objects.requireNonNull(pTarget);

    IConnector connector = null;
    switch(pConnectorType) {
    case Line:
        connector = new DocsExampleLineConnector(pDrawing, pSource, pTarget);
        break;
    case Curve:
        connector = new DocsExampleBezierConnector(pDrawing, pSource, pTarget);
        break;
    }

    connector.buildUI();
    pDrawing.addConnector(connector);

    return connector;
}
```

Also, in the builder's constructor, we set `pConnectorType` to be `Curve` by default.

With that all the pieces in place, if you run the application now, click on "New card" button and try to connect the `circle` card with the `rectangle` card, you will get this message in console:

```
java.lang.UnsupportedOperationException: Bezier connector can't connect to centered pin, only to edge pins.
```

Yay, it works! Well, not quite, eh? In order to use cubic Bezier connector we need two `rectangle` cards so you can either change the `newCard()` method in `DocsExampleHandler` to create two `rectangle` cards, or you can click "New card" button once, move one of the `rectangle` cards away, click "New card" button again and then connect the new card to the first one:

![][curved connector example]

> checkout `curved_connector` tag to see changes we made in this section

#### Wrapping it up

With all the UI pieces in place and components implemented, let's finish up by adding missing pieces of application logic which will make this little app of ours usable for creating simple graphs.

The first thing we'll do is to wire up two drop-down boxes so user can choose what type of card and connector to use as defaults.

To set a new card type, we implement `setNewCardType(String)` method in `DocsExampleHandler` class:

```java
public void setNewCardType(String type) {
    switch(type) {
    case "Circle":
        DocsExampleCreator.getInstance().setDefaultCardType(DocsCardType.Circle);
        break;
    case "Rectangle":
        DocsExampleCreator.getInstance().setDefaultCardType(DocsCardType.Rectangle);
        break;
    }
}
```

Before we handle drop-down for new connector type, we need to add setter and support for it in the `DocsExampleCreator` class:

```java
private ConnectorType defaultConnectorType = ConnectorType.Line;

public void setDefaultconnectorType(ConnectorType type) {
    defaultConnectorType = type;
}

@Override
public IConnector newDefaultConnector(IDrawingBoard drawing, IPin source, IPin target) {
    IConnector connector = connector()
            .setDrawingBoard(drawing)
            .setSource(source)
            .setTarget(target)
            .setType(defaultConnectorType)
            .build();
    return connector;
}
```

Notice that in `newDefaultConnector()` method we only added the `.setType(defaultConnectorType)` line.

Now, to set a new connector type, we implement `setNewConnectorType(String)` method in `DocsExampleHandler` class:

```java
public void setNewConnectorType(String type) {
    switch(type) {
    case "Line":
        DocsExampleCreator.getInstance().setDefaultconnectorType(ConnectorType.Line);
        break;
    case "CubicArrow":
        DocsExampleCreator.getInstance().setDefaultconnectorType(ConnectorType.Curve);
        break;
    }
}
```

We will also update `DocsExampleUI` class code to pre-select first entries in bot of our drop-down lists by adding these lines:

```java
cbCardChoices.getSelectionModel().selectFirst();
cbConnectorChoices.getSelectionModel().selectFirst();
```

to `wireEventHandling()` method, **after** we wired up change listener on both `ChoiceBox`es.

Now we can change `newCard()` method in `DocsExampleHandler` to this:

```java
public void newCard() {
    ICard newCard = DocsExampleCreator.getInstance().newDefaultCard(drawing);

    newCard.setPosition(new Point2D(50, 50));
}
```

It places all new cards at (50,50), which may be not ideal but is good enough for our example application.

You can now start the app and test the code by creating and connecting differnt card types with different connector types:

![][using choosers and wiring up]

And finally, to implement deleting selected card or connecor and setting up title of the selected card, we need to enlist help of the `CardSelectionManager`. This manager has been initialized by CardsFX toolkit and is involved in the works of our application behind the scenes. All we need to do is to ask it for currently selected card or connector.

With that in mind, implementations of the three remaining methods in `DocsExampleHandler` look fairly simple:

```java
public void deleteSelectedCard() {
    ICard card = CardSelectionManager.getInstance(drawing).getSelectedCard();
    if (card != null) {
        drawing.removeCard(card);
        CardSelectionManager.getInstance(drawing).setSelectedCard(null);
    }
}

public void deleteSelectedConnector() {
    IConnector connector = CardSelectionManager.getInstance(drawing).getSelectedConnector();
    if(connector != null) {
        drawing.removeConnector(connector);
        CardSelectionManager.getInstance(drawing).setSelectedConnector(null);
    }
}

public void setSelectedCardText(String text) {
    ICard card = CardSelectionManager.getInstance(drawing).getSelectedCard();
    if (card != null & card instanceof ITitledCard) {
        ((ITitledCard) card).setTitle( text );
    }
}
```

All you need to do is `getInstance()` of `CardSelectionManager`, `getSelectedCard()` or
`getSelectedConnector()` and then do something with the card or connector.

Finally, when you start the app we were building here, you can:

- select card type or connector type
- create new cards of selected type
- connect the cards with connector of selected type
- change title of the selected card
- delete selected card
- delete selected connector
- clear drawing board

![][final app]

> checkout `final_app` tag to see the code after changes we made in this section

## UI customization and CSS

All pre-defined CSS styles for CardsFX can be found in `css` package in [CardsFX source].

Style names are declared in the `Styles` class and that class has a static method `getDefaultCss()` which returns URL string of the `default.css` stylesheet you can use to load it up in your application, if you want to. `default.css` imports a number of other stylesheets:

```css
@import url("base.css");
@import url("card.css");
@import url("pin.css");
@import url("connector.css");
@import url("drawing.css");
```

each of which contains default style definitions needed for sample components to show correctly. You can look at those stylesheets to see examples of style definitions used by CardsFX.

CardsFX documentation names the CSS classes used by components and ot which part of the component's scene sub-graph are they assigned. Here is a quick summary:

| Component        | node             | CSs style             |
| ---------------- |:----------------:| ---------------------:|
| Drawing board    | ui node          | `drawing-pane`        |
| Card             | root             | `card`                |
|                  | content          | `card-content`        |
|                  | content          | `card-selected`       |
| Pin              | root             | `pin`                 |
|                  | connection point | `pin-connector`       |
| Connector        | connector shape  | `connector`           |
|                  | connector shape  | `connector-selected`  |
| Box selection    | rectangle        | `mouse-box-selection` |

Besides these, `Styles` class contains `connector-arrow-head` which is used by sample connector implementations.

`base.css` file in `css` package contains definition for `-cb-base` property used in styling sample cards, pins and connectors. You can override this in your application's css with any color value, if you are going to use CardsFX default styles, or you can set it to one of 4 predefined values:

- `-cb-base-color-orange`
- `-cb-base-color-purple`
- `-cb-base-color-green`
- `-cb-base-color-blue`

For our example application, we will make a couple of minor changes in `app.css` file, just for illustration.

First, we will change drawing board background image:

```css
.drawing-pane {
    -fx-background-image: url("graph_bg.png");
}
```

Then, we will remove the orange border around drawing board and set its size:

```css
.drawing-pane {
    -fx-pref-width: 1000;
    -fx-pref-height: 600;
    -fx-background-image: url("graph_bg.png");

    /* remove that ugly orange border */
    -fx-border-color: none;
}
```

And finally, we will select `green` as base color for our components by adding `-cb-base` property override in `.root` selector:

```css
.root {
    -fx-base: #373737;
    -cb-base: -cb-base-color-green;
}
```

Once all this is done, application will look something like this:

![][green cards]


[app structure overview]: images/app_structure_overview.png "Overview of sample app structure"
[initial app ui]: images/initial_app_ui.png "Initial app UI"
[packages]: images/packages.png "Packages"
[app with drawing board]: images/app_with_drawing_board.png "Drawing board added to the app"
[source structure 01]: images/source_structure_01.png "Source tree 01"
[two cards]: images/two_cards.png "Two cards"
[selectd card]: images/selected_card.png "One of two cards is selected"
[mouse selection]: images/mouse_selection.png "Mouse selection"
[cards with pins version]: images/cards_with_pins_version.png "Cards with pins version"
[added css for pins and card]: images/added_css_for_pins_and_card.png "CSS for pins and rectangle card"
[connected cards]: images/connected_cards.png "Connected cards"
[curved connector example]: images/curved_connector_example.png "Curved connector example"
[using choosers and wiring up]: images/using_choosers_and_wiring_up.png "Wired up chooser boxes"
[final app]: images/final_app.png "Final app view"
[green cards]: images/green_cards.png "Green card base color"

[builder pattern]: https://en.wikipedia.org/wiki/Builder_pattern
[abstract factory pattern]: https://en.wikipedia.org/wiki/Abstract_factory_pattern

[CardsFX source]: https://gitlab.com/zagortenej-cardsfx/com.ravendyne.cardsfx.git
[cardsfx-docexample]: https://gitlab.com/zagortenej-cardsfx/cardsfx-docexample.git
[CardsFx examples]: https://gitlab.com/zagortenej-cardsfx/cardsfx-docexample.git
