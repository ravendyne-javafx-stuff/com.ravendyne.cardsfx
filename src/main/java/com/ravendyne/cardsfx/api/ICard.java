package com.ravendyne.cardsfx.api;

import com.ravendyne.cardsfx.external.fxtoys.ui.DragContext;

import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.layout.Pane;

public interface ICard {

    int getCardId();
    
    void buildUI();

    Pane getRootNode();

    Node getContentNode();

    IDrawingBoard getDrawingBoard();

    IPin addPin(IPin pin);

    Iterable<IPin> getPins();

    /** Card's root node position relative to top-left corner of {@link IDrawingBoard drawing board} */
    Point2D getPosition();

    /** Card's root node position relative to top-left corner of {@link IDrawingBoard drawing board} */
    void setPosition(Point2D position);

    void setAsSelected();

    void setAsDeselected();

    DragContext getDragContext();

    void discard();

}
