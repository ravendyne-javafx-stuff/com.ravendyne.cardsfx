package com.ravendyne.cardsfx.api;

import javafx.scene.Node;

public interface IConnector {

    void buildUI();

    Node getRootNode();

    IDrawingBoard getDrawingBoard();

    IPin getSource();

    IPin getTarget();

    void updatePosition();

    void setAsDeselected();

    void setAsSelected();

    void discard();

}
