package com.ravendyne.cardsfx.api;

public interface IDefaultsBuilder {
    ICard newDefaultCard(IDrawingBoard drawing);
    IConnector newDefaultConnector(IDrawingBoard drawing, IPin source, IPin target);
}
