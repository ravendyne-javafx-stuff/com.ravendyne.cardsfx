package com.ravendyne.cardsfx.api;

import javafx.scene.layout.Pane;

public interface IDrawingBoard {

    Pane getNode();

    void addCard(ICard card);

    void removeCard(ICard card);

    void addConnector(IConnector connector);

    void removeConnector(IConnector connector);

    Iterable<ICard> getCards();

    Iterable<IConnector> getConnectors();

    IDefaultsBuilder getDefaultsBuilder();

    void setDrawingWidth(double width);

    void setDrawingHeight(double height);

    double getDrawingWidth();

    double getDrawingHeight();

    void clean();

}
