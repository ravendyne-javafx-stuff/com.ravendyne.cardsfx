package com.ravendyne.cardsfx.api;


public interface ILabeledConnector extends IConnector {
    
    String getLabel();
    
    void setLabel(String label);

}
