package com.ravendyne.cardsfx.api;

import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.Parent;

public interface IPin {
    
    public static enum Location {
        Top,
        Right,
        Bottom,
        Left,
        Center
    }
    
    void buildUI();

    int getPinId();

    Parent getRootNode();
    
    Node getConnectorAttachmentNode();

    ICard getCard();

    Location getLocation();

    void setLocation(Location location);
    
    /** Connector attachment point coordinates, in local coordinate space of this pin's connector attachment point node (returned by {@link #getConnectorAttachmentNode()}. */
    Point2D getAttachmentPositionLocal();

    /** Position of this pin's connector attachment point, relative to top-left corner of {@link IDrawingBoard drawing board}. */
    Point2D getAttachmentPosition();

    /** Position of this pin's connector attachment point node local coordinate space origin (0,0), relative to top-left corner of {@link IDrawingBoard drawing board}. */
    Point2D getAttachmentNodePosition();

    /**
     * Updates this pin's root node position on drawing board.
     * <p>
     * Implementation of this method should either update position of, or send position update notification to, a connector that is attached to this pin.
     * </p>
     * <p>
     * Since pins are usually attached to cards, and their position is managed by (via JavaFX mechanisms) a card it is attached to,
     * default implementation of this method does nothing about this pin's position.
     * </p>
     */
    void updatePosition();

    void connect(IConnector card);

    void disconnect(IConnector card);

    void discard();

}
