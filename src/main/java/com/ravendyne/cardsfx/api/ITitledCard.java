package com.ravendyne.cardsfx.api;


public interface ITitledCard extends ICard {

    String getTitle();

    void setTitle(String title);

}
