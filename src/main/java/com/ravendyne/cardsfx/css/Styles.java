package com.ravendyne.cardsfx.css;


public final class Styles {
    private Styles() {}

    public static final String MOUSE_BOX_SELECTION_STYLE = "mouse-box-selection";

    public static final String DRAWING_BOARD_STYLE = "drawing-pane";

    public static final String CARD_DEFAULT_STYLE = "card";
    public static final String CARD_CONTENT_STYLE = "card-content";
    public static final String CARD_SELECTED_STYLE = "card-selected";

    public static final String PIN_DEFAULT_STYLE = "pin";
    public static final String PIN_CONNECTOR_DEFAULT_STYLE = "pin-connector";

    public static final String CONNECTOR_DEFAULT_STYLE = "connector";
    public static final String CONNECTOR_SELECTED_STYLE = "connector-selected";
    public static final String CONNECTOR_ARROW_HEAD_STYLE = "connector-arrow-head";
    
    public static String getDefaultCss() {
        return Styles.class.getResource("default.css").toExternalForm();
    }
}
