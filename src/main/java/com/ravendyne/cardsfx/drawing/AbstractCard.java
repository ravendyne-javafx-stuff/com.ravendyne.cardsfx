package com.ravendyne.cardsfx.drawing;

import java.util.HashSet;
import java.util.Set;

import com.ravendyne.cardsfx.api.ICard;
import com.ravendyne.cardsfx.api.IDrawingBoard;
import com.ravendyne.cardsfx.api.IPin;
import com.ravendyne.cardsfx.css.Styles;
import com.ravendyne.cardsfx.external.fxtoys.ui.DragContext;
import com.ravendyne.cardsfx.managers.CardDraggingContext;

import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.layout.Pane;

public abstract class AbstractCard implements ICard {
    private static final boolean bDebug = false;

    private int cardId;
    
    private DragContext dragContext;
    private Set<IPin> pins;
    
    private IDrawingBoard drawingBoard;
    private Pane rootNode;
    private Node contentNode;

    public AbstractCard(IDrawingBoard drawing) {

        cardId = nextCardId();

        pins = new HashSet<>();

        drawingBoard = drawing;

    }

    protected abstract Pane createRootUINode();
    protected abstract Node createContentUINode();
    protected abstract void addPinToUI(IPin pin);
    protected abstract void removePinFromUI(IPin pin);

    @Override
    public void buildUI() {
        contentNode = createContentUINode();
        contentNode.getStyleClass().add( Styles.CARD_CONTENT_STYLE );

        rootNode = createRootUINode();
        rootNode.getStyleClass().add( Styles.CARD_DEFAULT_STYLE );
        
        dragContext = new CardDraggingContext(this);
        dragContext.setConsumeEvent( true );
        dragContext.addOnDraggingListener((delta) -> {
            pins.forEach((p) -> p.updatePosition());
        });
    }
    
    @Override
    public Pane getRootNode() {
        return rootNode;
    }

    @Override
    public Node getContentNode() {
        return contentNode;
    }

    @Override
    public void discard() {
        // don't discard discarded card
        // yes, indeed :)
        if(cardId == -1) {
            return;
        }

        // unregisters event listeners and clears all
        // registered listeners on drag context itself
        dragContext.discard();
        dragContext = null;

        // make sure pins are removed from this card
        // and then disposed of
        pins.forEach((p) -> {
            removePinFromUI( p );
            p.discard();
        });

        pins.clear();
        rootNode.getChildren().clear();

        // release reference we got via constructor
        drawingBoard = null;
        
        // these ended up outside of this instance eventually
        // and nullifying them here helps with debugging memory leaks
        rootNode = null;
        contentNode = null;

        // helpful hint that this card has been shredded
        cardId = -1;
    }

    @Override
    public IPin addPin(IPin pin) {
        if(pin.getCard() != this) {
            throw new IllegalArgumentException("Can't add pin that belongs to a different card");
        }

        addPinToUI(pin);

        pins.add(pin);
        if(bDebug) System.out.println("card #" + getCardId() + " -> added pin #" + pin.getPinId());
        
        return pin;
    }

    private int nextCardId() {
        return DefaultIdProvider.nextNodeId();
    }

    @Override
    public int getCardId() {
        return cardId;
    }
    
    @Override
    public DragContext getDragContext() {
        return dragContext;
    }

    @Override
    public void setPosition(Point2D position) {
        rootNode.relocate(position.getX(), position.getY());
        pins.forEach((p) -> p.updatePosition());
    }

    @Override
    public Point2D getPosition() {
        Point2D boxTopLeftOnCanvas = new Point2D( rootNode.getLayoutX(), rootNode.getLayoutY() );
        return boxTopLeftOnCanvas;
    }

    @Override
    public IDrawingBoard getDrawingBoard() {
        return drawingBoard;
    }

    @Override
    public Iterable<IPin> getPins() {
        return pins;
    }
    
    @Override
    public void setAsSelected() {
        contentNode.getStyleClass().add(Styles.CARD_SELECTED_STYLE);
    }
    
    @Override
    public void setAsDeselected() {
        contentNode.getStyleClass().remove(Styles.CARD_SELECTED_STYLE);
    }

    @Override
    public String toString() {
        String klassName = getClass().getName();
        String simpleName = klassName.substring(klassName.lastIndexOf('.')+1);

        return simpleName + "[id= " + getCardId() + "]";
    }
}
