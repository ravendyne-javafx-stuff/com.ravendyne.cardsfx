package com.ravendyne.cardsfx.drawing;

import com.ravendyne.cardsfx.api.IConnector;
import com.ravendyne.cardsfx.api.IDrawingBoard;
import com.ravendyne.cardsfx.api.IPin;
import com.ravendyne.cardsfx.css.Styles;
import com.ravendyne.cardsfx.managers.CardSelectionManager;

import javafx.scene.Node;

public abstract class AbstractConnector implements IConnector {

    protected IPin source;
    protected IPin target;
    protected IDrawingBoard drawing;

    private Node rootNode;
    private Node connectorShapeNode;

    protected AbstractConnector(IDrawingBoard drawing, IPin sourcePin, IPin targetPin) {
        this.drawing = drawing;

        source = sourcePin;
        target = targetPin;
        source.connect(this);
        target.connect(this);
        
    }

    protected abstract Node createRootUINode();
    protected abstract Node createConnectorShapeUINode();
    
    @Override
    public void buildUI() {
        connectorShapeNode = createConnectorShapeUINode();
        connectorShapeNode.getStyleClass().add(Styles.CONNECTOR_DEFAULT_STYLE);
        connectorShapeNode.setOnMousePressed((e) -> {
            CardSelectionManager.getInstance(drawing).setSelectedConnector(this);
            // consume event, otherwise underlying component might do something we don't want to
            // for example, drawing pane may deselect all cards/connectors on click
            e.consume();
        });

        rootNode = createRootUINode();

        updatePosition();
    }
    
    @Override
    public Node getRootNode() {
        return rootNode;
    }

    @Override
    public void discard() {
        if(connectorShapeNode == null) {
            return;
        }
        connectorShapeNode.setOnMousePressed(null);
        // used as indication that we have already disconnected
        connectorShapeNode = null;

        if(source != null ) {
            source.disconnect(this);
        }
        if(target != null ) {
            target.disconnect(this);
        }
        
        drawing.removeConnector(this);
        
        // release references received via constructor
        source = null;
        target = null;
        drawing = null;

        // not needed but helpful when debugging memory leaks
        rootNode = null;
    }

    @Override
    public IDrawingBoard getDrawingBoard() {
        return drawing;
    }

    @Override
    public IPin getSource() {
        return source;
    }

    @Override
    public IPin getTarget() {
        return target;
    }

    @Override
    public void setAsSelected() {
        if(connectorShapeNode != null) {
            connectorShapeNode.getStyleClass().add(Styles.CONNECTOR_SELECTED_STYLE);
        }
    }

    @Override
    public void setAsDeselected() {
        if(connectorShapeNode != null) {
            connectorShapeNode.getStyleClass().remove(Styles.CONNECTOR_SELECTED_STYLE);
        }
    }
}
