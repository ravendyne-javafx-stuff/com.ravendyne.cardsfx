package com.ravendyne.cardsfx.drawing;

import java.util.HashSet;
import java.util.Set;

import com.ravendyne.cardsfx.api.ICard;
import com.ravendyne.cardsfx.api.IConnector;
import com.ravendyne.cardsfx.api.IDrawingBoard;
import com.ravendyne.cardsfx.css.Styles;
import com.ravendyne.cardsfx.managers.CardSelectionManager;

import javafx.scene.Node;
import javafx.scene.layout.Pane;

public abstract class AbstractDrawingBoard implements IDrawingBoard {

    private Set<ICard> cards;
    private Set<IConnector> connectors;

    private Pane drawingBoardNode;

    public AbstractDrawingBoard() {
        cards = new HashSet<>();
        connectors = new HashSet<>();
        
        buildUI();
    }

    protected abstract Pane createUINode();
    protected abstract void addCardNodeToBoard(Node card);
    protected abstract void removeCardNodeFromBoard(Node card);
    protected abstract void addConnectorNodeToBoard(Node connector);
    protected abstract void removeConnectorNodeFromBoard(Node connector);

    private void buildUI() {
        drawingBoardNode = createUINode();

        drawingBoardNode.getStyleClass().add(Styles.DRAWING_BOARD_STYLE);
    }
    
    @Override
    public Pane getNode() {
        return drawingBoardNode;
    }

    @Override
    public void clean() {
        CardSelectionManager.getInstance(this).deselectAll();

        // make copy to avoid concurrent modification exception
        Set<IConnector> connectorsCopy = new HashSet<>(connectors);
        for (IConnector connector : connectorsCopy) {
            removeConnector(connector);
        }

        connectors = new HashSet<>();

        // make copy to avoid concurrent modification exception
        Set<ICard> cardsCopy = new HashSet<>(cards);
        for (ICard card : cardsCopy) {
            removeCard(card);
        }
        cardsCopy.clear();
        cards.clear();
    }
    
    @Override
    public void removeCard(ICard card) {
        if (cards.contains(card)) {
            CardSelectionManager.getInstance(this).stopManaging(card);
            cards.remove(card);
            removeCardNodeFromBoard( card.getRootNode() );
            card.discard();
        }
    }

    @Override
    public void addCard(ICard card) {
        if(card.getDrawingBoard() != this) {
            throw new IllegalArgumentException("Can't add a card from different drawing");
        }

        addCardNodeToBoard( card.getRootNode() );
        card.getRootNode().toFront();
        cards.add(card);
        CardSelectionManager.getInstance(this).startManaging(card);
    }

    @Override
    public void addConnector(IConnector connector) {
        if(connector.getDrawingBoard() != this) {
            throw new IllegalArgumentException("Can't add a connection from different drawing");
        }

        addConnectorNodeToBoard( connector.getRootNode() );
        // connector.toBack();
        connector.getRootNode().toFront();
        connectors.add(connector);
    }

    @Override
    public void removeConnector(IConnector connector) {
        if (connectors.contains(connector)) {
            connectors.remove(connector);
            removeConnectorNodeFromBoard( connector.getRootNode() );
            connector.discard();
        }
    }

    @Override
    public Iterable<ICard> getCards() {
        return cards;
    }

    @Override
    public Iterable<IConnector> getConnectors() {
        return connectors;
    }

    @Override
    public void setDrawingWidth(double width) {
        drawingBoardNode.setPrefWidth(width);
    }

    @Override
    public void setDrawingHeight(double height) {
        drawingBoardNode.setPrefHeight(height);
    }

    @Override
    public double getDrawingWidth() {
        return drawingBoardNode.getWidth();
    }

    @Override
    public double getDrawingHeight() {
        return drawingBoardNode.getHeight();
    }

}
