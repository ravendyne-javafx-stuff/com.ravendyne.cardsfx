package com.ravendyne.cardsfx.drawing;


import java.util.HashSet;
import java.util.Set;

import com.ravendyne.cardsfx.api.ICard;
import com.ravendyne.cardsfx.api.IConnector;
import com.ravendyne.cardsfx.api.IPin;
import com.ravendyne.cardsfx.css.Styles;
import com.ravendyne.cardsfx.managers.CardConnectionManager;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.css.PseudoClass;
import javafx.geometry.Point2D;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.layout.Region;

public abstract class AbstractPin implements IPin {
    private static final boolean bDebug = false;

    private static final PseudoClass HOVER_PSEUDO_CLASS = PseudoClass.getPseudoClass( "hover" );
    
    private int pinId;
    
    private Parent rootNode;
    private Node pinConnectionPointNode;
    private Set<IConnector> connectors;

    private ICard card;
    private Location location;
    
    protected AbstractPin(ICard card) {
        this.card = card;
        connectors = new HashSet<>();
        location = Location.Right;

        pinId = nextPinId();

    }

    protected abstract Region createRootUINode();
    protected abstract Node createConnectionPoinUINode();

    @Override
    public void buildUI() {
        pinConnectionPointNode = createConnectionPoinUINode();
        pinConnectionPointNode.getStyleClass().add(Styles.PIN_CONNECTOR_DEFAULT_STYLE);

        rootNode = createRootUINode();
        rootNode.getStyleClass().add(Styles.PIN_DEFAULT_STYLE);

        setupDragAndConnect();

        rootNode.layoutXProperty().addListener( (value, oldVal, newVal) -> {
            updatePosition();
        });
        rootNode.layoutYProperty().addListener( (value, oldVal, newVal) -> {
            updatePosition();
        });
        rootNode.layoutBoundsProperty().addListener( (value, oldVal, newVal) -> {
            updatePosition();
        });
    }

    @Override
    public Parent getRootNode() {
        return rootNode;
    }

    @Override
    public Node getConnectorAttachmentNode() {
        return pinConnectionPointNode;
    }

    @Override
    public ICard getCard() {
        return card;
    }
    
    @Override
    public Location getLocation() {
        return location;
    }

    @Override
    public void setLocation(Location location) {
        this.location = location;
    }

    private int nextPinId() {
        return DefaultIdProvider.nextNodeId();
    }
    
    @Override
    public int getPinId() {
        return pinId;
    }
    
    @Override
    public void connect(IConnector connector) {
        connectors.add(connector);
    }
    
    @Override
    public void discard() {
        if(card == null) return;
        
        // Indicates that the pin has already been discarded
        card = null;

        // avoid concurrent access, pins call our disconnect() method
        Set<IConnector> copy = new HashSet<>(connectors);
        copy.forEach((c) -> c.discard());
        connectors.clear();
        copy.clear();

        pinConnectionPointNode.setOnMousePressed(null);
        pinConnectionPointNode.setOnMouseReleased(null);
        pinConnectionPointNode.setOnDragDetected(null);
        pinConnectionPointNode.setOnMouseDragged(null);
        pinConnectionPointNode.setOnMouseDragOver(null);
        pinConnectionPointNode.setOnMouseDragEntered(null);
        pinConnectionPointNode.setOnMouseDragExited(null);
        pinConnectionPointNode = null;

        // not needed, GC should take care of it, but it help in debugging memory leaks
        rootNode = null;
        // another helpful hint when debugging memory leaks
        pinId = -1;
    }
    
    @Override
    public void disconnect(IConnector connector) {
        connectors.remove(connector);
    }

    @Override
    public void updatePosition() {
        // nothing to do for this pin, it should be added to box and managed there
        // notify connector though...
        if(!connectors.isEmpty()) {
            connectors.forEach((c) -> c.updatePosition());
        }
    }
    
    private Point2D getConnPosition(Point2D point) {
        Point2D pt = pinConnectionPointNode.localToParent(point);
        
        Parent p = pinConnectionPointNode.getParent();
        while (p != card.getRootNode()) {
            pt = pt.add( p.localToParent(Point2D.ZERO) );

            p = p.getParent();
        }
        
        pt = pt.add( card.getRootNode().localToParent(Point2D.ZERO) );
        
        return pt;
    }

    @Override
    public Point2D getAttachmentNodePosition() {
        Point2D pinPosition = getConnPosition(Point2D.ZERO);

        return pinPosition;
    }
    
    @Override
    public Point2D getAttachmentPosition() {
        Point2D pinPosition = getConnPosition(getAttachmentPositionLocal());

        return pinPosition;
    }

    private void setupDragAndConnect() {
        // https://docs.oracle.com/javase/8/javafx/api/javafx/scene/input/MouseDragEvent.html

        // indicates if mouse event occurred during drag gesture
        // used by MOUSE_RELEASED and MOUSE_DRAGGED event handlers
        SimpleBooleanProperty dragAndConnectInProgress = new SimpleBooleanProperty();

        // resets the drag-and-connect flag and prevents the event from being propagated to underlying node(s),
        // which is done so that drag gesture doesn't initiate dragging of underlying card
        pinConnectionPointNode.setOnMousePressed((e) -> {
            if(bDebug) System.out.println("pin #" + getPinId() +" mouse pressed");

            dragAndConnectInProgress.set(false);
            // consume MOUSE_PRESSED so that we don't start dragging the card itself
            e.consume();
        });
        
        // if we were dragging, finishes the drag-and-connect gesture by connecting
        // source and target nodes
        pinConnectionPointNode.setOnMouseReleased((e) -> {
            if(bDebug) System.out.println("pin #" + getPinId() +" mouse released");
            if(bDebug) System.out.println(e.getTarget());

            if(dragAndConnectInProgress.get()) {
                dragAndConnectInProgress.set(false);
                CardConnectionManager.getInstance(card.getDrawingBoard()).connect();
                card.getRootNode().getScene().setCursor(Cursor.DEFAULT);
            }
        });

        // starts full drag gesture and initiates our drag-and-connect one by
        // setting connection source pin and drag-and-connect gesture's line start point
        pinConnectionPointNode.setOnDragDetected((e)->{
            if(bDebug) System.out.println("pin #" + getPinId() +" start full drag");

            pinConnectionPointNode.startFullDrag();
            dragAndConnectInProgress.set(true);
            Point2D pinPosition = getAttachmentPosition();
            double x = pinPosition.getX();
            double y = pinPosition.getY();
            CardConnectionManager.getInstance(card.getDrawingBoard()).connectorLineStart(x, y);
            CardConnectionManager.getInstance(card.getDrawingBoard()).setSource(this);
            card.getRootNode().getScene().setCursor(Cursor.CROSSHAIR);
        });

        // if drag-and-connect gesture is in progress, updates drag-and-connect gesture's line end point
        pinConnectionPointNode.setOnMouseDragged((e)->{
            if(bDebug) System.out.println("pin #" + getPinId() +" mouse dragged");

            if(dragAndConnectInProgress.get()) {
                // we have to use pin's connection node position instead of attachment point
                // because mouse event coordinates are relative to the node local origin.
                Point2D pinPosition = getAttachmentNodePosition();
                double x = pinPosition.getX() + e.getX();
                double y = pinPosition.getY() + e.getY();
                CardConnectionManager.getInstance(card.getDrawingBoard()).connectorLineEnd(x, y);
            }
            // consume MOUSE_DRAGGED so that we don't start dragging the card itself
            e.consume();
        });
        
        pinConnectionPointNode.setOnMouseDragOver((e)->{
            if(bDebug) System.out.println("pin #" + getPinId() +" mouse drag over");
        });

        // sets the pin, that the drag gesture just entered, as a connection target pin
        // we also mark the pin as target in UI in some implementation specific way (i.e. by changing its background color)
        pinConnectionPointNode.setOnMouseDragEntered((e)->{
            if(bDebug) System.out.println("pin #" + getPinId() +" mouse drag ENTERED!");

            markAsConnectorTarget();
            CardConnectionManager.getInstance(card.getDrawingBoard()).setTarget(this);
        });

        // clears the pin, that the drag gesture just entered, as a connection target pin
        // we also clear the pin as target in UI in some implementation specific way (i.e. by reverting its background color)
        pinConnectionPointNode.setOnMouseDragExited((e)->{
            if(bDebug) System.out.println("pin #" + getPinId() +" mouse drag EXITED!");

            clearAsConnectorTarget();
            CardConnectionManager.getInstance(card.getDrawingBoard()).setTarget(null);
        });
    }
    
    public void markAsConnectorTarget() {
        pinConnectionPointNode.pseudoClassStateChanged( HOVER_PSEUDO_CLASS, true );
    }
    
    public void clearAsConnectorTarget() {
        pinConnectionPointNode.pseudoClassStateChanged( HOVER_PSEUDO_CLASS, false );
    }

    @Override
    public String toString() {
        String klassName = getClass().getName();
        String simpleName = klassName.substring(klassName.lastIndexOf('.')+1);

        return simpleName + "[id= " + getPinId() + ", location=" + location + ", parent=" + card.getCardId() + "]";
    }
}
