package com.ravendyne.cardsfx.drawing;

import java.util.concurrent.atomic.AtomicInteger;

public class DefaultIdProvider {
    private static AtomicInteger nextNodeId = new AtomicInteger(0);

    private DefaultIdProvider() {}
    
    public static int nextNodeId() {
        return nextNodeId.addAndGet(1);
    }

}
