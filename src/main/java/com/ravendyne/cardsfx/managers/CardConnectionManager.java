package com.ravendyne.cardsfx.managers;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import com.ravendyne.cardsfx.api.IDefaultsBuilder;
import com.ravendyne.cardsfx.api.IDrawingBoard;
import com.ravendyne.cardsfx.api.IPin;

import javafx.scene.paint.Color;
import javafx.scene.shape.Line;

public class CardConnectionManager {
    private static final boolean bDebug = false;
    public static final String CSS_CLASS = "dragging-connector";

    private static Map<IDrawingBoard, CardConnectionManager> instances = new HashMap<>();
    private IPin sourcePin;
    private IPin targetPin;
    
    private IDrawingBoard drawing;
    private Line draggingLine;

    private IDefaultsBuilder defaultsBuilder;

    private CardConnectionManager(IDrawingBoard drawing) {
        this.drawing = drawing;
        this.defaultsBuilder = drawing.getDefaultsBuilder();
        draggingLine = new Line();
        draggingLine.setVisible(false);
        draggingLine.setStroke(Color.DARKOLIVEGREEN);
        draggingLine.setStrokeWidth(1.0);
        draggingLine.getStrokeDashArray().addAll(Arrays.asList(20.0, 10.0));
        // have to set the line to be transparent to mouse events
        // otherwise it will just eat up all of them and the node that
        // we are trying to connect to will get no drag events.
        draggingLine.setMouseTransparent(true);
        this.drawing.getNode().getChildren().add(draggingLine);
    }
    
    public static CardConnectionManager getInstance(IDrawingBoard drawing) {
        CardConnectionManager instance = instances.get(drawing);

        if(instance == null) {
            instance = new CardConnectionManager(drawing);
            instances.put(drawing, instance);
        }
        
        return instance;
    }
    
    public void setSource(IPin pin) {
        sourcePin = pin;
    }
    
    public void setTarget(IPin pin) {
        targetPin = pin;
    }
    
    public void connect() {
        draggingLine.setVisible(false);

        if(sourcePin == null || targetPin == null) {
            if(bDebug) System.out.println("Nothing to connect");
        } else if(sourcePin.getPinId() != targetPin.getPinId()) {
            if(bDebug) System.out.println("Connect source #" + sourcePin.getPinId() + " to target #" + targetPin.getPinId());
            defaultsBuilder.newDefaultConnector(drawing, sourcePin, targetPin);
        } else {
            if(bDebug) System.out.println("Won't connect pin to itself");
        }
        sourcePin = null;
        targetPin = null;
    }

    public void connectorLineStart(double x, double y) {
        draggingLine.setStartX(x);
        draggingLine.setStartY(y);
        draggingLine.setEndX(x);
        draggingLine.setEndY(y);

        draggingLine.toFront();
        draggingLine.setVisible(true);

        if(bDebug) System.out.println("set line start @ " + x + " , " + y);
    }

    public void connectorLineEnd(double x, double y) {
        draggingLine.setEndX(x);
        draggingLine.setEndY(y);

        if(bDebug) System.out.println("set line end @ " + x + " , " + y);
    }
}
