package com.ravendyne.cardsfx.managers;

import com.ravendyne.cardsfx.api.ICard;
import com.ravendyne.cardsfx.external.fxtoys.ui.DragContext;

import javafx.geometry.Point2D;

public class CardDraggingContext extends DragContext {

    ICard card;

    public CardDraggingContext(ICard card) {
        super(card.getContentNode());
        this.card = card;
    }

    @Override
    protected void setNodePosition(Point2D position) {
        card.setPosition(position);
    }

    @Override
    protected Point2D getNodePosition() {
        return card.getPosition();
    }

}
