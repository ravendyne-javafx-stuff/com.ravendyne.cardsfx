package com.ravendyne.cardsfx.managers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.ravendyne.cardsfx.api.IConnector;
import com.ravendyne.cardsfx.api.ICard;
import com.ravendyne.cardsfx.api.IDrawingBoard;

import javafx.beans.property.ListProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Bounds;
import javafx.scene.input.MouseEvent;

public class CardSelectionManager {
    private static final boolean bDebug = false;
    

	private static Map<IDrawingBoard, CardSelectionManager> instances = new HashMap<>();

    private IDrawingBoard pane;
    
    // if only one card is selected, it is recorded here
    // if a group of cards is selected, the one that is being dragged
    // with mouse is recorded here
    private ObjectProperty<ICard> oneSelectedCard;
    // if a group of cards is selected, all of them are recorded here,
    // except for the one that is being dragged by mouse (oneSelectedCard)
    private ListProperty<ICard> manySelectedCards;
    private ObjectProperty<IConnector> selectedConnector;
    
    Map<ICard, EventHandler<? super MouseEvent>> cardMouseEventHandlers;

    private CardSelectionManager(IDrawingBoard pane) {
        this.pane = pane;

        cardMouseEventHandlers = new HashMap<>();
        setup();
    }
    
    public static CardSelectionManager getInstance(IDrawingBoard pane) {
    	CardSelectionManager csm = instances.get(pane);

    	if(csm == null) {
    		csm = new CardSelectionManager(pane);
    		instances.put(pane, csm);
    	}

    	return csm;
    }
    
    public void deselectAll() {
        manySelectedCards.clear();
        oneSelectedCard.set( null );
        selectedConnector.set( null );
    }

    private void setup() {
        selectedConnector = new SimpleObjectProperty<>();
        // changes selected connector style as it is selected/deselected
        selectedConnector.addListener((observable, oldVal, newVal) -> {
            if(oldVal != null) {
                oldVal.setAsDeselected();
            }
            if(newVal != null) {
                newVal.setAsSelected();
            }
        });

        ObservableList<ICard> observableList = FXCollections.observableArrayList(new ArrayList<>());
        manySelectedCards = new SimpleListProperty<>(observableList);
        // changes card styles as they are being added/removed to selection group
        manySelectedCards.addListener((ListChangeListener.Change<? extends ICard> c) -> {
            while(c.next()) {
                if(c.wasAdded()) {
                    for(ICard item : c.getAddedSubList()){
                        item.setAsSelected();
                    }
                } else if(c.wasRemoved()) {
                    for(ICard item : c.getRemoved()) {
                        item.setAsDeselected();
                    }
                }
            }
        });

        oneSelectedCard = new SimpleObjectProperty<>();
        // changes one/main selected card style as it is selected/deselected
        oneSelectedCard.addListener((observable, oldVal, newVal) -> {
            if(oldVal != null) {
                oldVal.setAsDeselected();
                if(bDebug) System.out.println( "De-selected " + oldVal);
            }
            if(newVal != null) {
                newVal.setAsSelected();
                if(bDebug) System.out.println( "Selected " + newVal);
            }
        });
        
        // when user clicks on drawing pane background, deselect all cards
        pane.getNode().addEventHandler(MouseEvent.MOUSE_PRESSED, (e) -> {
            deselectAll();
        });
        
        // create mouse box selection behaviour for the drawing pane whose selection we manage
        MouseBoxSelection mouseBoxSelection = new MouseBoxSelection( pane.getNode() );
        // update a list of selected cards when mouse group select gesture finishes
        mouseBoxSelection.setOnDragStop((bb) -> {
            if(bDebug) System.out.println( "Mouse box selection: " + bb );
            manySelectedCards.clear();
            for(ICard card : pane.getCards()) {
                // FIXME use content node to determine if we selected the card
                // content node boundsInParent won't work here since mouse bounding box is in drawing board's coordinate space
                if(bb.intersects( getContentBoundsInDrawing( card ) )) {
                    manySelectedCards.add( card );
                }
            }
        });
    }

    private Bounds getContentBoundsInDrawing(ICard card) {
        Bounds bb = card.getRootNode().getBoundsInParent();
        return bb;
    }
    
    public void startManaging(ICard card) {
        // when user clicks on a card, select it and prepare for dragging
        final EventHandler<? super MouseEvent> eventHandler = (e) -> {
            // group selection ?
            if(!manySelectedCards.isEmpty()) {
                if(manySelectedCards.contains(card) || oneSelectedCard.get() == card) {
                    // this (clicked) card is part of the group
                    // add back to the group whichever was the card under mouse
                    if(oneSelectedCard.get() != null) {
                        manySelectedCards.add(oneSelectedCard.get());
                    }
                    // remove this card from the group, we will promote it
                    // as the main selected card at the end of this method
                    manySelectedCards.remove(card);
                } else {
                    // this (clicked) card is NOT part of the group
                    manySelectedCards.clear();
                }
            }
            // set as the only, or as the group main, selected card
            oneSelectedCard.set(card);
            e.consume();
        };

        // bind card's selection related mouse event processing to its content node 
        card.getContentNode().addEventHandler(MouseEvent.MOUSE_PRESSED, eventHandler);
        cardMouseEventHandlers.put(card, eventHandler);
        
        // drag the whole card group, if any, as the main card is being dragged by mouse
        card.getDragContext().addOnDraggingListener((delta) -> {
            for(ICard otherCard : manySelectedCards) {
                otherCard.setPosition(otherCard.getPosition().add(delta));
            }
        });
    }

    public void stopManaging(ICard card) {
        if(oneSelectedCard.get() == card) {
            oneSelectedCard.set(null);
        }
        manySelectedCards.remove(card);
        // TODO card removeEventHandler MouseEvent.MOUSE_PRESSED ??
        card.getDragContext().addOnDraggingListener(null);
        card.getContentNode().removeEventHandler(MouseEvent.MOUSE_PRESSED, cardMouseEventHandlers.get(card));
        cardMouseEventHandlers.remove(card);
    }

    public ICard getSelectedCard() {
        return oneSelectedCard.get();
    }

    public void setSelectedCard(ICard newCard) {
        oneSelectedCard.set(newCard);
    }

    public void addSelectedCardChangeListener(ChangeListener<? super ICard> listener) {
    	// to avoid registering the same listener multiple times
        oneSelectedCard.removeListener(listener);
        oneSelectedCard.addListener(listener);
    }

    public IConnector getSelectedConnector() {
        return selectedConnector.get();
    }

    public void setSelectedConnector(IConnector newConnector) {
        selectedConnector.set(newConnector);
    }
}
