package com.ravendyne.cardsfx.managers;

import java.util.function.Consumer;

import com.ravendyne.cardsfx.css.Styles;

import javafx.event.EventHandler;
import javafx.geometry.BoundingBox;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Rectangle;

public class MouseBoxSelection implements EventHandler<MouseEvent> {
    private double mouseAnchorX;
    private double mouseAnchorY;
    private Consumer<BoundingBox> onDragStop;
    private boolean consumeEvents;
    
    Pane pane;
    Rectangle selectionBox;
    
    boolean wasDragged;

    public MouseBoxSelection(Pane pane) {
        this.onDragStop = null;
        this.pane = pane;
        this.wasDragged = false;
        
        selectionBox = new Rectangle();
        selectionBox.getStyleClass().add( Styles.MOUSE_BOX_SELECTION_STYLE );
        selectionBox.setVisible( false );

        pane.addEventHandler(MouseEvent.MOUSE_PRESSED, this);
        pane.addEventHandler(MouseEvent.MOUSE_DRAGGED, this);
        pane.addEventHandler(MouseEvent.MOUSE_RELEASED, this);
        
        pane.getChildren().add( selectionBox );
    }
    
    public void setConsumeEvent(boolean consume) {
        consumeEvents = consume;
    }
    
    public void setOnDragStop(Consumer<BoundingBox> onDragStop) {
        this.onDragStop = onDragStop;
    }
    
    public void start(MouseEvent event) {
        mouseAnchorX = event.getX();
        mouseAnchorY = event.getY();
        
        selectionBox.relocate( mouseAnchorX, mouseAnchorY );
        selectionBox.setWidth( 1.0 );
        selectionBox.setHeight( 1.0 );
        selectionBox.setVisible( true );
        selectionBox.toFront();
    }

    public void drag(MouseEvent event) {
        double newWidth = event.getX() - mouseAnchorX;
        double newHeight = event.getY() - mouseAnchorY;
        updateBoxPosition( newWidth, newHeight );
    }
    
    private void updateBoxPosition( double newWidth, double newHeight ) {
        if(newWidth > 0) {
            if(newHeight > 0) {
                // +w, +h
                selectionBox.relocate( mouseAnchorX, mouseAnchorY );
                selectionBox.setWidth( newWidth );
                selectionBox.setHeight( newHeight );
            } else {
                // +w, -h
                selectionBox.relocate( mouseAnchorX, mouseAnchorY + newHeight );
                selectionBox.setWidth( newWidth );
                selectionBox.setHeight( -newHeight );
            }
        } else {
            if(newHeight > 0) {
                // -w, +h
                selectionBox.relocate( mouseAnchorX + newWidth, mouseAnchorY );
                selectionBox.setWidth( -newWidth );
                selectionBox.setHeight( newHeight );
            } else {
                // -w, -h
                selectionBox.relocate( mouseAnchorX + newWidth, mouseAnchorY + newHeight );
                selectionBox.setWidth( -newWidth );
                selectionBox.setHeight( -newHeight );
            }
        }
    }

    @Override
    public void handle(MouseEvent event) {
        if(event.getEventType() == MouseEvent.MOUSE_PRESSED) {
            start(event);
        } else if(event.getEventType() == MouseEvent.MOUSE_DRAGGED) {
            wasDragged = true;
            drag(event);
        } else if(event.getEventType() == MouseEvent.MOUSE_RELEASED) {
            selectionBox.setVisible( false );
            if(wasDragged && onDragStop != null) {
                wasDragged = false;
                onDragStop.accept( new BoundingBox( 
                        selectionBox.getLayoutX(), selectionBox.getLayoutY(), 
                        selectionBox.getWidth(), selectionBox.getHeight() ) );
            }
        }
        if(consumeEvents) event.consume();
    }

}
