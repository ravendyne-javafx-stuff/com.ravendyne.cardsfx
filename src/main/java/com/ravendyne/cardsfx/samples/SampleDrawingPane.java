package com.ravendyne.cardsfx.samples;

import com.ravendyne.cardsfx.api.IDefaultsBuilder;
import com.ravendyne.cardsfx.drawing.AbstractDrawingBoard;

import javafx.scene.Node;
import javafx.scene.layout.Pane;

public class SampleDrawingPane extends AbstractDrawingBoard {
    private final static double initialWidth = 1024.0;
    private final static double initialHeight = 768.0;

    public SampleDrawingPane() {
        super();
    }

    @Override
    protected Pane createUINode() {
        Pane drawingPane = new Pane();

        drawingPane.setPrefWidth(initialWidth);
        drawingPane.setPrefHeight(initialHeight);
        
        return drawingPane;
    }

    @Override
    protected void removeCardNodeFromBoard( Node card ) {
        getNode().getChildren().remove(card);
    }

    @Override
    protected void addCardNodeToBoard( Node card ) {
        getNode().getChildren().add(card);
    }
    
    @Override
    protected void addConnectorNodeToBoard( Node connector ) {
        getNode().getChildren().add(connector);
    }
    
    @Override
    protected void removeConnectorNodeFromBoard( Node connector ) {
        getNode().getChildren().remove(connector);
    }

    @Override
    public IDefaultsBuilder getDefaultsBuilder() {
        return SamplesCreator.getInstance();
    }

}
