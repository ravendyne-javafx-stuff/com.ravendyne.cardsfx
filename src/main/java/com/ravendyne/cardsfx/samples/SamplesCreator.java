package com.ravendyne.cardsfx.samples;

import com.ravendyne.cardsfx.api.ICard;
import com.ravendyne.cardsfx.api.IConnector;
import com.ravendyne.cardsfx.api.IDefaultsBuilder;
import com.ravendyne.cardsfx.api.IDrawingBoard;
import com.ravendyne.cardsfx.api.IPin;
import com.ravendyne.cardsfx.api.IPin.Location;
import com.ravendyne.cardsfx.samples.cards.CardBuilder;
import com.ravendyne.cardsfx.samples.connectors.ConnectorBuilder;
import com.ravendyne.cardsfx.samples.connectors.ConnectorBuilder.ConnectorType;
import com.ravendyne.cardsfx.samples.pins.PinBuilder;

public final class SamplesCreator implements IDefaultsBuilder {
    private static SamplesCreator instance;
    
    private static ConnectorType defaultConnectorType = ConnectorType.Cubic;

    private SamplesCreator() {}
    
    public static SamplesCreator getInstance() {
        if(instance == null) {
            instance = new SamplesCreator();
        }
        
        return instance;
    }

    public PinBuilder pin() {
        return new PinBuilder();
    }

    public CardBuilder card() {
        return new CardBuilder();
    }

    public ConnectorBuilder connector() {
        return new ConnectorBuilder();
    }

    public void setDefaultConnectorType(ConnectorType type) {
        defaultConnectorType = type;
    }

    @Override
    public ICard newDefaultCard(IDrawingBoard drawing) {
        ICard card = card().setDrawing(drawing).setTitle("new card").setText("new card").build();

        pin().setCard(card).setLocation(Location.Left).build();
        pin().setCard(card).setLocation(Location.Right).build();
        pin().setCard(card).setLocation(Location.Top).build();
        pin().setCard(card).setLocation(Location.Bottom).build();
        
        return card;
    }

    @Override
    public IConnector newDefaultConnector(IDrawingBoard drawing, IPin source, IPin target) {
        IConnector connector = connector()
                .setDrawingBoard(drawing)
                .setSource(source)
                .setTarget(target)
                .setType(defaultConnectorType)
                .build();
        return connector;
    }

}
