package com.ravendyne.cardsfx.samples.cards;

import java.util.Objects;

import com.ravendyne.cardsfx.api.ICard;
import com.ravendyne.cardsfx.api.IDrawingBoard;

public class CardBuilder {
    private static final boolean bDebug = false;
    
    protected IDrawingBoard pDrawing;
    protected String pTitle;
    String pText;

    public CardBuilder setDrawing(IDrawingBoard drawing) {
        Objects.requireNonNull(drawing);
        pDrawing = drawing;
        return this;
    }

    public CardBuilder setTitle(String title) {
        Objects.requireNonNull(title);
        pTitle = title;
        return this;
    }

    public CardBuilder setText(String text) {
        pText = text;
        return this;
    }

    public ICard build() {
        Objects.requireNonNull(pDrawing);
        Objects.requireNonNull(pTitle);

        ICard card = new CardWithText( pDrawing, pTitle, pText );
        card.buildUI();
        pDrawing.addCard(card);
        if(bDebug) System.out.println("added card #" + card.getCardId());

        return card;
    }

}
