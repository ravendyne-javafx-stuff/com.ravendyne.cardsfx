
package com.ravendyne.cardsfx.samples.connectors;

import java.util.Objects;

import com.ravendyne.cardsfx.api.IConnector;
import com.ravendyne.cardsfx.api.IDrawingBoard;
import com.ravendyne.cardsfx.api.IPin;

public class ConnectorBuilder {
    private static final boolean bDebug = false;
    
    public static enum ConnectorType {
        Line,
        LineArrow,
        Cubic,
        CubicArrow,
    }

    private IDrawingBoard pDrawing;
    private IPin pSource;
    private IPin pTarget;

    private ConnectorType pConnectorType;

    public ConnectorBuilder setDrawingBoard(IDrawingBoard drawing) {
        Objects.requireNonNull(drawing);
        pDrawing = drawing;
        pConnectorType = ConnectorType.Cubic;
        return this;
    }

    public ConnectorBuilder setSource(IPin sourcePin) {
        Objects.requireNonNull(sourcePin);
        pSource = sourcePin;
        return this;
    }

    public ConnectorBuilder setTarget(IPin targetPin) {
        Objects.requireNonNull(targetPin);
        pTarget = targetPin;
        return this;
    }

    public ConnectorBuilder setType(ConnectorType connectorType) {
        Objects.requireNonNull(connectorType);
        pConnectorType = connectorType;
        return this;
    }

    public IConnector build() {
        Objects.requireNonNull(pDrawing);
        Objects.requireNonNull(pSource);
        Objects.requireNonNull(pTarget);

        IConnector connector = null;
        switch(pConnectorType) {
            case Line:
                connector = new LineConnector(pDrawing, pSource, pTarget);
                break;
            case LineArrow:
                connector = new LineConnectorWithArrow(pDrawing, pSource, pTarget);
                break;
            case Cubic:
                connector = new BezierConnector(pDrawing, pSource, pTarget);
                break;
            case CubicArrow:
                connector = new BezierConnectorWithArrow(pDrawing, pSource, pTarget);
                break;
        }

        connector.buildUI();
        pDrawing.addConnector(connector);
        if(bDebug) System.out.println("added connector from #" + connector.getSource().getPinId() + " to #" + connector.getTarget().getPinId());

        return connector;
    }

}
