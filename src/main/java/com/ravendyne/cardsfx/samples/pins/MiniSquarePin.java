package com.ravendyne.cardsfx.samples.pins;


import com.ravendyne.cardsfx.api.ICard;
import com.ravendyne.cardsfx.drawing.AbstractPin;

import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class MiniSquarePin extends AbstractPin {

    private Color color;
    
    private final double connectorSize = 8;
    
    MiniSquarePin(ICard card, Color color) {
        super( card );

        this.color = color;
    }

    @Override
    public void discard() {
        super.discard();

        // release the reference
        color = null;
    }

    @Override
    protected Region createRootUINode() {
        HBox pinUI = new HBox();
//        pinUI.getStyleClass().add( "debug-region" );

        pinUI.setAlignment(Pos.CENTER_LEFT);
        pinUI.getChildren().add(getConnectorAttachmentNode());
        
        pinUI.setMaxSize(connectorSize, connectorSize);
        
        return pinUI;
    }
    
    @Override
    protected Node createConnectionPoinUINode() {
        Rectangle connector = new Rectangle(connectorSize, connectorSize);
        connector.setStroke(color);
        return connector;
    }

    @Override
    public String toString() {
        String klassName = getClass().getName();
        String simpleName = klassName.substring(klassName.lastIndexOf('.')+1);

        return simpleName + "[id= " + getPinId() + ", location=" + getLocation() + ", parent=" + getCard().getCardId() + "]";
    }

    @Override
    public Point2D getAttachmentPositionLocal() {
        return new Point2D(getConnectorAttachmentNode().getLayoutBounds().getWidth() / 2.0, getConnectorAttachmentNode().getLayoutBounds().getHeight() / 2.0);
    }
}
