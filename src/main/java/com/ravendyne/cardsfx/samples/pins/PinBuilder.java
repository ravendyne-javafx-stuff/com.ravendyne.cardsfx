package com.ravendyne.cardsfx.samples.pins;

import java.util.Objects;

import com.ravendyne.cardsfx.api.ICard;
import com.ravendyne.cardsfx.api.IPin;
import com.ravendyne.cardsfx.api.IPin.Location;

import javafx.scene.paint.Color;

public class PinBuilder {

    ICard pCard;
    Color pColor;
    Location pLocation;
    
    public PinBuilder() {
        pColor = Color.web("#00a8f4");
    }
    
    public PinBuilder setCard(ICard card) {
        Objects.requireNonNull(card);
        pCard = card;
        return this;
    }

    public PinBuilder setColor(Color color) {
        Objects.requireNonNull(color);
        pColor = color;
        return this;
    }

    public PinBuilder setColor(String colorString) {
        pColor = Color.web(colorString);
        return this;
    }

    public PinBuilder setLocation(Location location) {
        Objects.requireNonNull(location);
        pLocation = location;
        return this;
    }

    public IPin build() {
        Objects.requireNonNull(pCard);
        Objects.requireNonNull(pColor);
        Objects.requireNonNull(pLocation);

        IPin pin = new MiniSquarePin(pCard, pColor);
        pin.buildUI();
        pin.setLocation(pLocation);
        pCard.addPin(pin);

        return pin;
    }

}
